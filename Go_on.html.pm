#lang pollen
◊define-meta[title]{Go on}
◊define-meta[author]{Elis Bondim}
◊define-meta[date]{13.06.2020}

Não adianta me culpar
Por erros do passado,
Sendo que nada há
Que possa mudá-lo. 
Cicatrizar é um processo 
Necessário e bem-vindo,
Não por ser o que eu quero,
Mas porque transforma o fim em início.
Talvez seja fisiológico
E requeira um tempo natural,
Mas também há em seu funcionamento próprio
Uma economia de não conservação do mal:
Se você decide alimentar mágoa
E se agarrar à dor,
À lagrima que deságua
No mesmo peito onde houve amor,
Em vez do aconchego
Que um dia te aqueceu,
Do carinho, abraço e chamego
Que um dia essa mesma pessoa te deu, 
Você assim escolhe a infelicidade!
Pois pensar que tudo que se viveu 
Foi em vão, mera passagem
É rejeitar parte do que é inevitavelmente teu.
É tua estória, tua memória,
Vivência que te constitui.
O que está em jogo é a tua trajetória
E tua chance de se alegrar com o que dela flui. 
Olhar pra trás e se sentir bem,
Satisfeito e pleno com o vivido
Não é um presente a outro alguém,
Mas a ti mesmo em teu íntimo. 
Enfeitar mais ainda as lembranças
Ou tirá-las todas as belezas,
Seguir a vida em sua dança
Ou desprezá-la por suas incertezas. 
Dois caminhos, talvez duas sentenças:
Escolher ser feliz a qualquer custo 
E a alegria de qualquer maneira
Ou viver eternamente em luto. 
Cada um cria um sentido 
Conforme a sua força, 
Sem precisar de sacrifício, 
Buscando que o amor-próprio não morra.  
Pessoas mudam o tempo todo 
E ainda está em tempo pra você também
Aceitar as novidades do que veio 
E do que ainda vem. 
Ficam aqui registradas minhas palavras
Porque um adeus não tem de ser ferida.
Pode ser belas portas até escancaradas,
Se você olhar a vida como sua amiga. 