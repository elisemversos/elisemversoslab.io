#lang pollen
◊define-meta[title]{Elis}
◊define-meta[author]{Elis Bondim}
◊define-meta[date]{25.06.2020}


A vida me fez assim:
Menos racional do que eu gostaria.
Mas o que seria de mim
Se não fosse essa alma em poesia?
Dizem que o poeta sempre sofre
Na mesma medida em que é feliz,
No sentir tão intenso e forte,
Esse sentir tão sinônimo de ‘Elis’.
Eu me escancaro e me fecho
Com facilidade.
Eu não me impeço,
Mas também não dou continuidade.
Eu me interesso e desinteresso
Num piscar de olhos,
Eu sorrio choro e rezo
E a vida emana dos meus poros.
Eu deixo um rastro,
Marcas sinais e indícios
Por onde passo,
Mas eu não fico.
Pra eu ficar, 
É preciso muito.
Que todo doar
Seja recíproco.
A vida é múltipla
E eu olho pra trás
E vejo sem culpa
Tudo que me faz
Em pouco tempo me abrir
Pra me trancar
Ir ao encontro pra depois fugir
E me isolar.
A vida sempre segue
E eu a deixo fluir,
O curso do rio prossegue
E eu só pertenço a mim.
