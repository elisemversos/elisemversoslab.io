#lang pollen
◊define-meta[title]{Quero fazer amor por aí}
◊define-meta[author]{Elis Bondim}
◊define-meta[date]{Março 2020}


Eu quero fazer amor
Sobre a grama orvalhada,
Sem receio sem temor,
Em uma noite enluarada. 

Que sobre a chuva nossos corpos
Se virem do avesso,
Se encharquem de vários modos
E ericem todos os meus pelos. 

Nas águas de um doce rio
Ou no alto de uma montanha,
Faça sol ou faça frio,
Que tudo nos atraia a essa dança. 

Essa dança de dois 
Nos mais diferentes ritmos;
Que não deixemos pra depois
Cada oportunidade disso. 

Com o vento nos descabelando,
Fora ou entre quatro paredes,
Com o sol nos queimando,
Até que as bocas sintam sede. 

Sobre areia terra mar,
Nem o céu é um limite.
Que a gente viva pra se amar,
Que todo cenário a gente pinte. 

Que a gente pinte como quiser
Com as cores que nos aprouver
Conscientes da obra-prima que é
Estar junto seja lá onde se estiver.
