#lang pollen
◊define-meta[title]{Esse drink}
◊define-meta[author]{Elis Bondim}
◊define-meta[date]{22.08.2020}
◊define-meta[audio]{Esse_drink.ogg}

Eu aprecio cada gole 
Desse drink que tenho em mãos.
Ele simboliza a minha sorte,
Minha direção.
As escolhas que eu fiz
E que eu faço todo dia,
Sou eu, só eu, Elis 
Na minha própria companhia. 
Ninguém é bem vindo 
A sentar ao meu lado,
A não ser que esteja indo 
Pro mesmo barco.
O barco mais maduro
Em que se pode embarcar, 
Em que é gostoso estar juntos 
E só se está pra somar. 
Não tô aqui pra ser falta,
Muito menos pra sentir. 
Não sou porto que acalma,
Nem mar em fúria a invadir. 
Eu sou fluxo e água
Deliciosa na qual nadar, 
Líquido que deságua
Em mim mesma a jorrar.
Eu me espalho,
E não espelho.
Sou toda a potência em ato,
Eu mesma me mereço.
Sou navegação
Curta ou longa,
Sem ou com direção,
Sensata ou louca. 
Só não tenho tempo
Pra jogar fora. 
Até gosto do que não entendo
E sou do tipo que se esforça. 
Mas só quero comigo
Quem se esforça também. 
Quem é o próprio melhor amigo
E não busca isso em ninguém. 
Eu tô aqui pra ser plus
Porque eu já me basto. 
I'm not here for us,
Não sou cobertor acolchoado. 
Eu sou água gelada,
Que manda a real. 
Mas também dá aquela esquentada
No corpo que segue o canal 
E sintoniza a energia
Pra escorrer pelos poros
Amor leveza harmonia,
Almas e corpos.
Se você entrar na minha onda,
Entra pra se molhar. 
E vê se não desonra 
A energia que eu te entregar.
Porque eu tô aqui pra quebrar 
Todo o calçadão,
Essa longa faixa de areia a separar 
Razão e emoção. 
Se você não tem essa capacidade,
O problema não tá em mim. 
Sou só natureza em intensidade,
Que ama acima de tudo a si. 
Eu lambo minha própria boca
Pra curtir esse gosto 
De tequila tangerina e força
Cuja gota escorre pelo meu rosto 
E cai sobre a minha roupa,
Essa roupa que eu abriria
E despiria toda
Pra minha própria alegria. 
Ah esse drink bem aqui na minha saliva 
É sinal de quem eu sou:
Minha melhor companhia 
A todo lugar que eu vou.