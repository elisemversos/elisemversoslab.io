#lang pollen
◊define-meta[title]{Entre luz e sombras}
◊define-meta[author]{Elis Bondim}
◊define-meta[date]{12.10.2020}
◊define-meta[audio]{Entre_luzes_e_sombras.ogg}

Entre luz e sombras
Eu me mostro, eu me escondo.
Quem eu sou ainda me assombra,
Mas eu me assumo me expondo:
Eu sou quem eu sou, 
Uma mulher a se criar,
Entendendo onde estou
E onde quero estar.
Corpo-alma a se narrar
Em poemas fotos escolhas.
Sob a luz a me iluminar,
Eu não fico mais na encolha.
Eu me mostro porque eu me quero
A iluminar o mundo afora,
A poesia a imagem a espírito o sexo,
O amanhã o ontem e o agora.
Eu não gosto do pouco,
Mas sei que aos poucos é que se alcança
Aquilo que nos deixa loucos
E também nos dá o fio da esperança:
O que queremos ser,
A mudança que queremos engendrar
No ser que habita mais do que se vê
E sim o íntimo que sob a sombra está.
Entre luzes e sombras,
Eu sou corpo desejo versos e sorrisos.
E sou tomada pelo que me soma
Entre impressões palavras e instintos.
Ainda há partes sombreadas
Jamais vistas,
Aguardando para que sejam observadas
Por olhos de quem também cria
Aquilo que quisermos ser
Juntos num espaço-tempo,
Na imagem-movimento do viver
Só pro nosso contento.
Essa mulher nada pronta
Mas a constantemente se transformar

