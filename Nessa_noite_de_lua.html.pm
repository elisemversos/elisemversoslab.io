#lang pollen
◊define-meta[title]{Nessa noite de lua}
◊define-meta[author]{Elis Bondim}
◊define-meta[date]{26.08.2020}
◊define-meta[audio]{Poema_Nessa_Noite_de_Lua.ogg}

Estou aqui
Sentada escrevendo,
Ainda te sentindo em mim,
Ainda te querendo.
Nessa noite enluarada
Em que vejo da minha janela
Essa rainha iluminada,
Que reflete na superfície dela
A luz do mundo 
E a catarse que somente à noite
- De madrugada, sobretudo – 
Bate em mim como açoite
Que corta, mas do jeito que eu gosto
Porque todo amante da vida
É amante também do desgosto
Da dor da lágrima do prazer e alegria.

Nessa noite enluarada
A lua me alcança,
A mim aqui agasalhada
Com uma alma que balança
Entre a saudade do teu sorriso
- Esse sorriso meia lua - 
E o ir embora sem aviso,
Desistindo de ser tua.
Sob o luar eu imagino
Que a gente evolua,
Mas também sinto
O medo de que dilua. 
Mas eu ainda desejo
A confluência
Do querer do sentimento,
Da fluência.

Eu não sou que nem a lua
Porque eu tenho minha própria luz,
Mas também desejo a luz tua
Pra me somar no que me conduz.
Eu sou diferente desse astro
Porque eu sou meu próprio centro.
E quero quem lado a lado,
Em decisão e em contento,
Gire nesse mundo vasto,
Sendo comigo dois corpos celestes
Que, em constante contato,
Sejam como a aura que veste 
A beleza estelar,
Ocupando o céu inteiro,
Enluarado ou solar
- companheiros. 