#lang pollen
◊define-meta[title]{O vizinho}
◊define-meta[author]{Elis Bondim}
◊define-meta[date]{15.09.2020}
◊define-meta[audio]{Poema_O_vizinho.ogg}
◊i{(O vizinho} ou ◊i{O cara da academia)}

Eu lhe sinto a me provocar 
Com seu jeito de malandro,
Esse seu costumeiro olhar 
De quem flerta e fica enrolando. 
Eu lhe vejo e disfarço
O tesão que sinto nas suas tatuagens,
O quanto me atraem seus braços,
E a partir daí eu sou toda viagem...
Eu viajo imaginando suas mãos 
Entre minhas pernas seios boca.
E ali em meio ao salão,
Eu queria seus dedos na minha coxa.
E vou toda me molhando 
Enquanto tô ali por perto;
Quem me vê me vê malhando,
Mas na mente eu sou só sexo. 
E o suor escorre e me encharca,
Enquanto o desejo me umedece também.
E eu lhe imagino na minha casa 
E não sei porque ele não vem. 
Mas não faz mal não
Porque, se é verdade que viver é melhor,
Também é gostosa a imaginação
- ela nunca me deixa só. 
Então eu me volto a ela
E sinto vontade de saber seu gosto,
Como ele é quando pega
Pro desejo dele um corpo. 
E essa sua barba, ah eu a queria
 Roçando na minha pele 
Até ficar bem vermelhinha,
Só pra ficar como quem pede
Uns beijos molhados pra sarar
A irritação carnal do atrito
Desse cara a quem eu vejo trabalhar
E que é também meu vizinho.
Tão perto e tão longe,
Eu continuo a lhe olhar.
Ele meio que se esconde,
E eu nem ligo, fico aqui a viajar.