#lang pollen
◊define-meta[title]{Fruto não proibido}
◊define-meta[author]{Elis Bondim}
◊define-meta[date]{07.06.2020}
◊define-meta[audio]{Fruto_não_proibido.mp3}

Eu posso me tocar
- eu sei que posso.
Eu sou minha namorada 
E meu eu é onde moro. 
Eu sou dona do meu corpo 
E não faz nenhum sentido
Alimentar um pudor 
Que foi por outros construído.
Sou como fruta do paraíso,
Só que eu não sou pecado.
Não sou fruto proibido:
Aqui o toque é liberado. 
Mas meu corpo grita mesmo
É pela tua mão:
Teus longos e gostosos dedos
- eles sim me dão tesão.
Quero você a adentrar meu íntimo
Até o fim do caminho,
Num movimento com o ritmo
Que te for pedido.
Eu quero que você me desbrave,
Enquanto eu sinto o teu cheiro. 
Quero sentir que você me invade
Como se colocasse teu braço inteiro
Dentro de mim,
Num dos lugares mais ricos
Que eu posso dar a ti.
E é desse jeito que eu fico
Quase perdendo o controle
De tanto que te quero 
Num vai e vem que me consome.
E é te desejando que espero 
Um dia de cada vez,
Na expectativa de nos termos perto 
Até chegarmos à nudez
Porque desejo assim beijar-te a boca
E percorrer teus traços,
Enquanto você me toca
Comigo em teus braços
Até eu dizer chega
E desejar o próximo passo.
E que a gente esteja
Os dois no mesmo compasso,
Pedindo-nos com os olhos,
Ansiando-nos com o pulsar 
Da excitação dos órgãos
Que fazem a mente girar.
Que a gente quase se implore
De tanta vontade um do outro
Como quem após longo jejum morde
O mais saboroso dos frutos. 
Que assim a gente se saboreie 
Com o prazer dos mortais,
E que você em mim entre 
Cada vez mais. 
