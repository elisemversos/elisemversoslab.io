#lang pollen
◊define-meta[title]{À tua vida}
◊define-meta[author]{Elis Bondim}
◊define-meta[date]{Maio 2018}
(Para P.)

Todos nós vivemos batalhas internas,
Das quais o mundo nem desconfia.
Fazemos do nosso eu um complexo de cavernas,
Onde escondemos a nossa essência.

Mas se o heroi não é somente aquele que sobrevive ao final,
O sábio, para vir a sê-lo, tem de ultrapassar muitas guerras.
A experiência acumulada é o canal
Que leva à fortaleza onde o eu agora se encerra.

Da transformação da caverna em fortaleza, porém,
Há um longo percurso a ser transcorrido.
Parece difícil, mas não desistas, meu bem...
Hoje não vale à pena ser heroi sem estar vivo.

Antes um sábio que carregue a angústia da vida,
Mas também a profunda capacidade do sentir.
Não te deixes abalar tanto por esse mundo em que pouco se estima
Esse teu dom mais raro, que faz ser tão especial o teu existir.

Quem foi o culpado que te determinou o que é normal?
Quem te impôs que quem não segue a norma é estranho?
A tua unicidade é o que há em ti de mais especial.
Não deixes que te diminuam com juízos tão tacanhos. 

O mundo tem por hábito classificar de esquisito o que não sabe explicar.
Mas talvez o esquisito não seja quem se sente estranho no ninho,
E sim quem não sabe a tua singularidade admirar.
Toda essa sociedade que tenta te aprisionar em regras - logo tu, que és passarinho!

Será que a tua alma tão singela não pode se relevar a tua maior preciosidade?
Será que o vento que agora te toca não pode te trazer novas perspectivas?
Não te cortes as tuas possibilidades,
Não assassines tuas pulsões tão vivas.

Tua complexidade é tua distinção.
Ela própria é o grande elemento que te faz ser quem tu és.
Pode ser tua arma, teu escudo e proteção.
É tua base, tua coluna, teus pés. 

Não, eu não acredito! Eu me recuso a acreditar
Que alguém capaz de passar essa energia tão forte
Pode mesmo desejar o fluxo do sangue parar,
E ter como solução a morte.

Não, eu não posso aceitar  
Que alguém capaz de sentir tão intensamente,
Queira deixar de sentir e acabar
Com o que te faz ser tão belo e tão brilhante.

Meu bem, não desistas, não te prives do sol que toca a tua pele,
Nem das mãos que anseiam por ti.
Faças com que a tua extraordinária força de sentimentos te vele
E te leve embora esse desejo de fim.

Meu bem, não prives as bocas que te desejam.
Nem teus ouvidos que se fascinam pelos mais variados sons.
Não prives os corpos que te anseiam,
Nem tua mente tão viva que, não, eu não creio que ela queira o sono.

Sei que nada posso te pedir,
E que não há controle sobre o desejo de romper com vida que parece ferida.
Mas não posso deixar de expressar isso que há dentro de mim,
Ainda que te influenciar talvez seja muita presunção minha.

Mas certamente é também uma dor pela tua dor,
Uma impotência que transformo em versos.
Uma necessidade minha à qual não posso me opor,
Preciso te dizer tudo isso... porque eu te prezo!

Que tu jogues no abismo o que te coloca nele,
E te fortaleças através disso que te pareces – só parece – ser a tua fraqueza.
Que tu não esqueças que pode ser bom sentir, tocar, enxergar... nem desse
Pedido de paciência, declaração de esperança e de admiração da tua grandeza.
