#lang pollen
◊define-meta[title]{O Beijo que eu quero te dar}
◊define-meta[author]{Elis Bondim}
◊define-meta[date]{Abril 2020}

(Para B.)

Aquele beijo a mais que eu não te dei 
Ainda está na minha boca
Reservado pra quando eu te vir
Se o sim for tua escolha. 

Quero que ele seja bem demorado
Pra eu sentir cada detalhe da tua língua 
Que, num movimento molhado,
Vai se extasiar com a minha. 

E que esse beijo, ao terminar, 
Desperte o desejo de outro;
Que meus lábios, sem pestanejar,
Se direcionem ao teu encontro. 

E que no décimo beijo
Nossas mãos não se contenham 
Pois, em busca do que almejo,
Minhas mãos já te desejam. 

Que tua mão me percorra
E me apalpe com força
Até que, crescentemente, eu toda
Esteja a te dizer: - "disponha". 

Que a espera valha à pena
 E o futuro seja bem aproveitado 
De modo que a gente só sinta e veja
Os nossos corpos reciprocamente  dados. 

Que a gente se demore no vão da porta 
E nos degraus da escada 
E que aonde a imaginação nos mova
A gente curta a estada. 

Pois o estar junto é um convite
Que, sob a chuva, sob a lua,
Querendo que o novo nos desafie,
Se faz à minha pele nua. 

Sob o frio, sob o sol;
O mundo inteiro lá fora.
Eu e você a sós
Sensações que me extrapolam.

Quando eu te encontrar novamente
Quero meu beijo na tua pele 
E o quer que a gente invente
Já é gosto que me apetece. 
