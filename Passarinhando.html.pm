#lang pollen
◊define-meta[title]{Passarinhando}
◊define-meta[author]{Elis Bondim}
◊define-meta[date]{04.02.2020}

Bem-te-vi
No céu índigo
Gaivotando
Que nem pipa.

Pairavas
E revoavas
Andorinhando
Em luz solar.

Vi periquito
Pequenino
Nadando
No céu marinho.

Calopsitei
Pelo canário
E o observei
No azul cenário.

 Quando um colibri,
Me coloriu
E me cobriu
De verde mar.

Pavoniei-me
Com toda a beleza 
Quetzal
Azul turquesa.

Borboletei-me
Com o traçado
Das tuas asas,
Faisão Dourado.

E tu me dissestes:
- Beija-flor
Pra colher o néctar
Azul esplendor.

- Mas o néctar que quero-
-quero 
É o teu.
Respondi.

É pelo teu bico
Que meu pólen anseia
Ser colhido,
Bem me queira. 

Abraçar-te em pétalas
E te florescer
Exalar essências belas
Que te dêem prazer.

Pássaro do paraíso,
Que venhas a mim,
Flor-de-lis
Azul Bondi.