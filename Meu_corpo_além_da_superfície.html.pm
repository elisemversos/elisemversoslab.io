#lang pollen
◊define-meta[title]{Meu corpo: além da superfície}
◊define-meta[author]{Elis Bondim}
◊define-meta[date]{12.10.2020}
◊define-meta[audio]{Inocência_-_meu_corpo.ogg}

Inocência, inocência
Eu não sinto falta da inocência.
Ela faz com que a gente não entenda
As nossas próprias vivências...
Não sou inocente,
Tampouco a mais sagaz das mulheres.
Sou gente como toda a gente,
Se descobrindo ao descobrir o que quer.
Estou expondo o que tantas desejam,
Mas não o fazem temendo
Os discursos dos que nos querem presas,
Cheias de amarras nos contendo.
Meu corpo é só um corpo.
Pra alguns é sinal do belo,
Não tanto para outros,
Mas definitivamente é mais do que sexo.
Meu corpo me leva aonde eu vou
Com essas minhas pernas grossas,
Minha memória lembra-se do que passou,
Como se minha mente tivesse várias portas
Nas quais eu adentro com meu pensamento,
Que me faz refletir sobre tudo,
E meus olhos me trazem o contentamento
De enxergar as cores do mundo.
Ah e as minhas mãos
Com elas eu seguro entre os meus dedos
Objetos corpos até uma fatia de pão
E tudo se acopla aos causos meus.
Minha língua sente o tato
De outras línguas, 
Mas também dos adorados
Dulçores amargores sucos frutas.
E os meus ouvidos
Ah eles me permitem ir ao céu!
Com as músicas e os timbres mais bonitos
Aos quais a minha voz se soma como véu:
Detalhe transparente que mantém,
Mas que altera na alegria da união,
Como alegre sinal de quem
Canta pra se sentir parte da canção.
Meus cabelos não existem
Só para serem puxados,
Eles são fios que insistem
Em me conduzir com o vento ondulado
Que eu sinto bater em minha pele
Ah! A minha pele
Ela não existe pro prazer alheio:
Ela é meio de contato do que se revele
Fonte do prazer meu!
E os meus seios, esses seios fartos
Não existem pra satisfazer bocas,
Existem pro que eu sentir com o tato
Daquilo que me toca.
E o meu ventre, meu poderoso ventre
Tem o poder da vida:
De fazer nascer gente,
Minha cria.
Ah o meu corpo
Ele existe pra mim mesma!
Pra todo encontro
Que eu mesma queira.

