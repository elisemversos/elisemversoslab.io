#lang pollen

◊define-meta[title]{Sobre a grama}
◊define-meta[author]{Elis Bondim}
◊define-meta[date]{22.05.2020}
◊define-meta[audio]{Sobre_a_grama__.wav}

Sobre a grama deitei meu corpo
De modo a sentir cada folha;
Lá imaginei teu rosto
E sonhei em ser a tua escolha. 
Imaginei-me desfazendo
O laço da minha roupa 
Sob o teu olhar atento,
Tão forte que me açoita,
Mas que me é tão instigante
Que me faz abrir o fecho
Mostrando o que o tecido esconde, 
Rompendo qualquer receio
De querer ser vista
Até deixar que se caiam 
As alças que antes me vestiam
Permitindo que saiam
Quaisquer resquícios de pudor 
Assumindo que eu quero 
Sobre essa grama fazer amor 
E a volúpia com que te espero 
É das minhas maiores riquezas,
A qual eu quero te dar 
Ao me dar por inteira 
No simples ato de deitar. 
Aqui em meio à essa mata
Eu quero as tuas mãos
Tua boca molhada
E toda a tua imaginação
Sobre os meus seios 
A me lambuzar 
E depois quero que os teus dedos 
Devagar se ponham a avançar
Na direção do meu íntimo mais íntimo 
Que pra você se abre
Como o mais gostoso dos caminhos 
Pro qual te dou livre passe.
Aqui sobre essa grama 
Eu quero as minhas mãos 
Minha boca molhada 
E toda a minha imaginação
Sobre a tua pele excitada 
A te lambuzar 
Quero minha língua minha saliva 
A se direcionar
Ao teu íntimo mais íntimo 
Que você o dê a mim
Às minhas mãos e ao ritmo 
E à minha boca, enfim,
Pra que a gente reciprocamente
Se ame e se presenteie 
Sobre esse verde tão presente 
E tão natural quanto o que a gente sente. 