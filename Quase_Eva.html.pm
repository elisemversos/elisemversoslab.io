#lang pollen
◊define-meta[title]{Quase Eva}
◊define-meta[author]{Elis Bondim}
◊define-meta[date]{23.05.2020}
◊define-meta[audio]{Quase_Eva_.wav}

Ao deitar sobre a canga
Quase me senti Eva
Ali onde a luz me alcança
E eu brilho em meio à relva.
Porém, logo me lembrei, 
De que essa tradição chama de pecado
O aspecto mais natural do nosso ser,
Que deveria ser o mais sagrado.
Pois não é dispêndio inútil de energia 
- pelo contrário,
É gerador de vida,
E condição do extraordinário. 
Afinal, a nossa existência
Só assim é possível. 
E através dessa potência
Nos tornamos o ato mais rico:
Somos toda a possibilidade
E todo o risco também,
Pro mal e pra bondade,
Impulsionamos o homem a ir além
De barreiras naturais
E das que ele próprio cria 
É só na matéria e concretude carnais
Que o homem age e modifica.
É assim que viemos ao mundo 
E é assim que nele continuaremos
Se a ele dermos um filho 
No qual permaneceremos.
Pertencemos à natureza
Mais que a qualquer metafísica,
Que por mais que nos enriqueça,
Às vezes também nos retira
Essa parte forte da vida 
Que nos ensinam a negar,
Às vezes a deixamos esquecida
E hoje eu tô aqui pra lembrar. 