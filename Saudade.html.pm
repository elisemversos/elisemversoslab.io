#lang pollen
◊define-meta[title]{Saudade}
◊define-meta[author]{Elis Bondim}
◊define-meta[date]{31.03.2020}

(Para B.)

Não deveria ser possível
Criar faísca de afeto por alguém
E desejar encontrá-lo,
Para em seguida ficar sem
Qualquer perspectiva de abraçá-lo,
Presos a um calendário imprevisível!
Não deveria ser permitido
Vivermos uma dimensão paralela
Em que uma atmosfera se instaura,
Para termos de abrir mão dela
E daquele gostinho de alma,
Logo em seguida ao que foi vivido!
Mas se não fosse uma possibilidade,
E se fosse proibido,
Tampouco teria sido realidade
Aquilo de que agora estamos desprovidos!
A saudade é ambígua:
Misto de desejo pelo que passou,
E de um lamento cuja força é exígua,
Pois no final reina a alegria do que ficou!
A saudade é uma vontade
Ela é um quero mais
De tornar ato o que a brevidade
Novamente em potência jaz.
O tempo é perigoso
- Grande amigo ou armadilha -
E constitui o risco
Que permeia toda a vida!
Ele trará os sinais
Do que será.
Mas não poderá jamais
O que teria sido nos contar.
A saudade tem algo de belo,
É um valorar positivo.
É guardar com carinho singelo
Momentos fugazes e infinitos:
Foram fugazes na duração
E são infinitos na saudade!
É a alegria de não ter dito não
E do sim em toda sua plasticidade!
A saudade, enfim,
É estímulo e querer
Que se faz presente em mim
Ao pensar naquele alguém!
31.03.2020

