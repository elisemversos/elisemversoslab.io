#lang pollen

◊define-meta[title]{S/T}
◊define-meta[author]{Elis Bondim}
◊define-meta[date]{23.03.2020}

(Para B.)

Gosto do modo como as tuas mãos
Me apertam com força até doer.
Essa dor me traz uma sensação
De intenso prazer.

Parecem prestes a arrancar minha pele,
Porém eu me sinto tão desejada,
Que isso faz com que o ato se revele
Uma delícia inesperada.

Às vezes parece que vão me quebrar todinha
E torcer meu pescocinho
E eu me sinto tão fragilzinha,
Mas querendo mais e muito.

Há uma certa agressividade
Nesse jogo erótico
Em que aguentar é uma vaidade
Que transforma o dolorido em ótimo.

É uma violência sensual
Que altera seu significado.
É um ato consensual
E um teatro do meu agrado.

Os vermelhos na minha pele nua
São registros dos teus dedos,
Que com força extenuam
Cada um dos meus medos.

São como tatuagem,
Porém passam após um tempo.
Trazem lembranças que agem
Sobre meu corpo em pensamento.

No sexo não há regras.
É preciso liberdade,
Que só tem quem não nega
Cada instinto que invade;

Invade a alma a pele a boca
A língua os pêlos e as mãos
E a vontade não é pouca
De libera-lo sem senãos.

Dois corpos que se unem
Com um mesmo objetivo:
Que no tempo que dure
Seja gostoso o que for vivido.

Minhas limitações
Existem para serem superadas.
E as surpresas, com poucas exceções,
São por mim abraçadas.

Conheço um pouco mais de mim
A cada sim que digo,
E neste movimento sem fim
Eu me permito ir sentindo.
