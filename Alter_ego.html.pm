#lang pollen
◊define-meta[title]{Alter ego}
◊define-meta[author]{Elis Bondim}
◊define-meta[date]{Janeiro 2020}

Ela tem o corpo em flores
E cabelos solares.
Ela tem todas as cores
Refletidas nos olhares.

As pétalas sobre a pele
Exalam um perfume doce
E o suor que escorre
Sob a roupa se esconde. 

Ela tem dentes Colgate
E piercing na sobrancelha.
Nas unhas um ouro esmalte,
No rosto a boca vermelha. 

Na mente natureza e arte
E planos pro futuro.
Não há nada que ela descarte,
Ela gosta do inseguro. 

Cantarola MPB
E espalha sorrisos,
Alucina como LSD
E tem poucos amigos. 

Ela curte adrenalina
E quer ter uma moto.
Ela flerta com meninas
E adora posar pra foto.

Ela tem curvas perigosas
Nas quais é fácil se perder.
Ela diz palavras gostosas
Só pra nos enlouquecer. 

Ela gosta de mato
E de nadar nua no mar.
Ela tem vários gatos
E quer o mundo para amar. 

Fumar um Beck
Em paz na cachu
E um calor que seque
O seu corpo nu.

Contar borboletas
E as estrelas do céu,
E as regras caretas
Ela as joga ao léu.

Ela me inspira,
Eu quase a respiro:
Como todo artista
Eu amo o que eu crio. 

Ela é uma imagem mental
Que eu visualizo e descrevo
Faço até seu mapa astral
Só limitá-la não me atrevo. 
