#lang pollen
◊define-meta[title]{Triste dia}
◊define-meta[author]{Elis Bondim}
◊define-meta[date]{29.02.2020}

Há dia
Em que é difícil
E a alegria
Parece impossível.

Eu me recolho
Atrás de um livro
Ou de página em branco
Onde faço abrigo.

Respiro fundo
Buscando paz.
Contudo,
Ela não se faz.

Um redemoinho
Me consome
E eu quase grito,
Mas o medo me come -

Come minha língua
E alguns sonhos.
Mas sei que nessa vida
Só a morte é um ponto.

O hoje é uma vírgula
Que logo passa.
E a tristeza também anima
À alma que se basta.

Porque é sinal
Do humano.
Não é um mal -
Eu não reclamo.

Só preciso purgar
Esta tempestade
E me reencontrar
Nessa dor que me invade.

Não posso mais
Me reconhecer em outrem.
Atracar-me como a um cais
Onde quer que eu me encontre.

Eu preciso
Ser meu pilar.
Construir isso
Também é me libertar.

Não é saudável
Depender do outro.
Não é viável
Viver n'outro.

A minha vida
Está em mim
A mão estendida
Não irá vir.

Ela é a minha própria -
Tem de ser.
Aceitar já é uma vitória
Que me guia a viver.

Não há porto seguro
Fora do meu eu.
E mesmo o que for duro
É só meu.

A lágrima
Me pertence
E ávida
Escorre quente.

O receio
Me pertence.
E sem freio
O agarro de frente.

A solidão
Me pertence.
E eu não digo não
A ela, tão pulgente.

Daqui a uns minutos
Tudo terá passado.
Então respiro profundo
E eu mesma me abraço.