#lang pollen
◊define-meta[title]{Incompreensão}
◊define-meta[author]{Elis Bondim}
◊define-meta[date]{21.07.2020}

Ele não entende, não, ele não entende 
Que quando eu falo de paixão
Falo de uma energia que mexe com a gente
E nos faz alçar vôo sem direção.
Ele não entende, não, ele não entende 
Que quando eu falo de sexo,
É a primeira vez que meu corpo sente 
Tudo isso que agora eu quero. 
O amor é uma consequência
Que pode ou não acontecer.
Mas honey, o desejo também é potência
Pra tudo que pode vir a ser!
Por que ter medo de dizer 
Que me quer ou me deseja?
Que risco há tão grande pra você
Em proferir o que não é sentença,
Muito menos promessa,
Só palavras que viram a cabeça
E tornam a vida mais poética!?
Se tenho de pagar pra ver 
Pra estar com quem não sabe se sente,
Por que você não paga um pouco também
E se arrisca a me dizer algo que positive minha mente? 
Se toda relação é uma troca,
Por que você não me dá um pouco mais de vontades 
Já que eu te dou todas as que me tocam
E me preenchem as noites em claro e ansiedade? 
Mas ele não entende 
Que não, não há futuro
Se não houver presente;
E tudo é risco, lá no fundo!
Por isso um dia de cada vez
Se faz necessário
E podemos hoje viver 
Sem projetar o longo prazo. 
Isso sim é ausência de contrato:
Que você sinta algo hoje
E não tenha medo de depois ser o contrário,
Mas que seja autêntico e que conte!
Conte muito nas decisões
Nas frases escolhas olhares,
Mesmo que depois se partam corações
E mais cacos se estilhacem.
Mas o hoje tem de existir,
Não em nome do amanhã,
Não pra atingir um fim
Cuja idealização pode ser vã.
Podemos sentir agora 
O que a alma apontar.
Não é da boca pra fora:
É da boca do momento a se realizar!
Não posso não consigo 
Ignorar todo o meu querer.
Por isso já disse e novamente deixo dito:
Algum sentimento tem de haver em você! 
Pra que faça sentido 
O hoje pelo hoje,
O agora imprevisível
Pelo que é e não pelo que será ou fosse. 
Honey, não sei porque você não entende 
Que eu não anseio compromisso,
Não há assinatura nem nada entre a gente
Nada além do presente breve e fugidio. 
E não, meu bem, isso não nos impede
De sentirmos algo no peito.
E se em você nada te impele
A vir ao encontro do meu beijo,
Então não, não faz sentido
Que você venha até tão longe.
Mas se você sente um pouquinho
Então você tem de dizer no hoje. 
Não é no daqui a um ano,
Nem daqui a uma eternidade.
Até lá o sentimento já é outro 
Pro bem pro mal pra todas as possibilidades. 
Não, não há probabilidades
Nem medidas ou disputas.
Há apenas uma necessidade
De que eu sinta você na minha e eu na tua,
Numa reciprocidade mínima,
Que vá muito além de nos acharmos 
Pessoas legais inteligentes ou amigas.
Que seja por hoje nos querermos e nos desejarmos,
Não como desejamos a quem passa na esquina.
Mas com um pouco mais de ardor,
Uma imaginação que pinta 
E tem espírito inventor. 
Eu misturo palavras 
Pra misturar minha alma 
Ao mundo que todo dia se instaura 
Com pressa urgência ou calma.
Mas diferente e diverso,
Inesperado e artístico,
Que me traz até o seu reverso 
Mas também o seu lado infinito. 
E nessa infinidade sem contagem,
Podia haver uma perspectiva
De que houvesse em você um pouco mais de coragem 
Pra me querer de maneira mais assertiva. 
Esperanças são só crenças
E eu alimentei as minhas,
Uma boba ingênua ou criança
Mas que certamente te abriria 
Tudo que houvesse pra se abrir 
Se você tivesse se aberto 
A si próprio a nós a mim 
Abraçando muito mais o incerto 
A ponto de não temer mostrar 
O que não é garantido nem fácil trajeto,
Mas crucial pra gente se reencontrar. 