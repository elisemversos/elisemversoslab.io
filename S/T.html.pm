#lang pollen
◊define-meta[title]{S/T}
◊define-meta[author]{Elis Bondim}
◊define-meta[date]{22.07.2020}

Está vendo essa blusa rosinha
De florzinhas e botões?
Ela cobre minha pele branquinha
E esconde minhas reações,
O que o meu corpo faz
Ao te sentir se aproximar,
Sobretudo se for por trás
E de surpresa, sem eu esperar.
Ela esconde os bicos dos meus seios,
Que ficam arrebitados
Ao sentir o leve percorrer dos teus dedos
Na iminência do contato.
Ela disfarça o frio na espinha
E os meus pelos eriçados,
A vontade da tua pele na minha,
Que deixa meu instinto ouriçado
Ao sentir o que te faz homem
Bem firme contraindo o meu quadril,
Junto ao teu abdome
E eu me sentindo a mil.
Eu gosto do calor do teu hálito
Bem na minha nuca,
Deixando todo o meu íntimo excitado
Até a minha boca buscar a tua.
Teu calor me incendeia
E me dá vontade de pegar tuas mãos,
Que por mim passeiam
E levá-las em lentidão
Por debaixo da minha blusa
Até alcançarem lá em cima,
Ao que no meu delírio você segura
Com vontade bem decidida.
E eu me molho e me jorro
Querendo de frente te sentir
E aí já não me controlo
E quero mesmo é abrir
E deixar cair ao chão
Esse tecido que disfarça
Mas que na minha imaginação
É a peça cuja falta
Mudaria todo o contexto,
Criaria outra estória,
Outro poema outro desfecho
Diferente desse de agora.
