#lang pollen
◊define-meta[title]{TPM na Quarentena}
◊define-meta[author]{Elis Bondim}
◊define-meta[date]{27.06.2020}
◊define-meta[audio]{Tpm_na_quarentena__.wav}

Eu choro e eu rio e eu rio e eu choro, 
Em cinco minutos vivo todas as emoções!
Eu peço eu falo eu berro eu imploro, 
Eu perco as estribeiras e todas as direções!
Eu respiro eu me acalmo eu suavizo e penso
E de novo eu estouro e perco o controle 
Eu me fascino me encanto me surpreendo e estremeço
E emano depois sugo e me levo já não sei aonde! 
Eu me enrolo e tropeço no meu próprio cadarço
E eu salto e caio de bunda no chão
Quebro a cara rasgo a blusa e me escancaro
Não me contenho não me privo sou só doação ! 
Em poucos segundos vou da água ao vinho
E me sinto bem me sinto mal bem muito bem mal muito mal 
Eu devoro a mim mesma e ao que tá no caminho 
Vou mordendo comendo sendo canibal 
E mastigo mastigo rápido ou devagarinho
Pra depois deglutir digerir extrair 
Sentir o gosto e guardar a memória na boca 
Refletir interiorizar aceitar e seguir
E me sentir desbaratada e louca!  
Eu amo eu odeio eu gosto eu desejo 
Eu te amo eu me amo eu quero nos amar! 
Eu fecho os olhos e te sinto te anseio te vejo 
E ao mesmo tempo sou alma revolta a vagar! 
Eu não te deixo eu me perco eu não me encaixo 
Eu me encontro mas me enraiveço por não te encontrar
E num lance que parece incrivelmente mágico
Eu durmo e acordo sentindo bem estar! 
