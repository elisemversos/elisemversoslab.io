#lang pollen

◊define-meta[title]{Como a um bom vinho}
◊define-meta[author]{Elis Bondim}
◊define-meta[date]{16.05.2020}
◊define-meta[audio]{Como_a_um_bom_vinho.mp3}

Quero que você me beba
Como a um bom vinho
Pois como uma taça a que se preencha
Meu corpo se enche de líquido
Um líquido que me molha toda
Às vezes branco, às vezes tinto
O qual eu deixo que entre minhas pernas escorra
E que se apodere de todos os meus sentidos 
Porque como a água da uva,
Esse suco feminino 
Que de dentro jorra até a vulva 
Tem cheiro, gosto e brio 
E com o paladar da tua língua
Você pode se embriagar 
E teu tato na minha pele nua 
Pode me elevar 
Cada parte da minha intimidade
Tem um modo de reagir 
À essa gostosa umidade
Que você acrescenta a mim
Com a saliva da tua boca 
Me tocando por inteira 
Uma mão apertando minha coxa
Até me deixar vermelha
A outra sobre o meu seio
Que eu te dou com vontade
Porque já cansei dos freios
Eu quero mesmo é a catarse
Da qual esse poderoso fluido 
É símbolo forte e sagrado
De conexão com o infinito
Potente e afirmativo phatos
Que pode transformar toda a sintaxe
De dois corpos que são linguagem
A se comunicar um com o outro
No amor, na sacanagem
Nesse jogo que é um acordo
Entre duas partes
Interessadas no prazer
Dando à tapa as suas faces
Pra se ascender
Da terra ao céu corpóreo
Com esse licor da natureza
Que inebria como o alcoólico
E é símbolo da vida e da beleza 
Do encontro entre dois 
Que entram um no outro
Com o mais flexível dos músculos
Com o dedo com o falo e todo o corpo
Enquanto houver fôlego.