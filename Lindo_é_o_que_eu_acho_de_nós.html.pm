#lang pollen
◊define-meta[title]{Lindo é o que eu acho de nós}
◊define-meta[author]{Elis Bondim}
◊define-meta[date]{2012}

Lindo é o que eu acho de nós

Minha alma encontrou a sua no instante de um abraço.
Nosso abraço eternizou minha alma no instante do encontro.
Minha alma, ao abraçar a sua, constituiu profundo laço,
Que em um errôneo passo, formou um nó e um desencontro.

O desencontro me fez abraçar a amargura.
A amargura me fez agir tolamente.
A lembrança do abraço me faz chorar de ternura.
Na amargura eu desacreditei do que você sente.

No ato de desacreditar me rebaixei ao infantil.
No ato infantil me descobri uma criança.
Ao duvidar de seus princípios me fiz vil.
Ao perceber minha atitude perdi qualquer esperança.

Na perda de esperança a felicidade me figurou impossível.
Na visão do impossível a vida passou a ser morte.
Na sensação de desfalecimento me descobri ainda sensível.
Na capacidade ainda viva de sentir me percebi mais forte.

Minha força se desvela no sentimento que em mim há.
O qual não irei negar com o fim, o qual suporta o sofrimento.
Se tudo passa, que passe ao seu tempo; sem nada a isso forçar.
E que fique concretizado no meu peito o nosso lindo sentimento.