#lang pollen

◊define-meta[title]{9.}
◊define-meta[author]{e.e. cummings}
◊define-meta[date]{◊span{from the ◊i{erotic poems} (2010)}}

there are so many tictoc
clocks everywhere telling people
what toctic time it is for
tictic instance five toc minutes toc
past six tic

Spring is not regulated and does
not get out of order nor do
its hands a little jerking move
over numbers slowly

                    we do not
wind it up it has no weights
springs wheels inside of
its slender self no indeed dear
nothing of the kind.

(So,when kiss Spring comes
we'll kiss each kiss other on kiss the kiss
lips because tic clocks toc don't make
a toctic difference
to kisskiss you and to
kiss me)


◊; Local Variables:
◊; mode: pollen
◊; comment-start: "◊; "
◊; End:
