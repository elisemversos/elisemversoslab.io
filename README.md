### adicionar novo poema

criar arquivo com extensão `html.pm` e colocar o seguinte cabeçalho:

```
#lang pollen
```

opcionalmente pode-se especificar alguns metadados:

```
◊define-meta[title]{9.}
◊define-meta[author]{e.e. cummings}
◊define-meta[date]{1949-05-12}
◊define-meta[audio]{9..mp3}
```

título é obrigatório, o resto não. o arquivo de áudio com a gravação
deve estar na pasta `media`.

o resto do arquivo é o conteúdo do poema: estrofes são separadas por
uma linha em branco.

para que o poema apareça no site ele precisa estar listado no arquivo
`index.ptree`.


### adicionar novo ícone

inicialmente estava usando caracteres unicode como ícones, mas em
computadores que não possuem fontes que os renderizavam eles não
aparecem (são substituídos por � ou pelo tofu). então usamos fonte
especial criada automaticamente em
https://icomoon.io/app/#/select/font usando somente os ícones que
precisamos, e carregamos as fontes em `icon-fonts/` usando o CSS em
`icon-font.css`. pra inserir um ícone criamos um elemento (usualmente
o `span`) com a classe tendo o nome do ícone, e o CSS inclui o ícone
automaticamente.
