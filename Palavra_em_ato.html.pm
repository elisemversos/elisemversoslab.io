#lang pollen
◊define-meta[title]{Palavra em ato}
◊define-meta[author]{Elis Bondim}
◊define-meta[date]{10-05-2020}
◊define-meta[audio]{Palavra_em_ato.mp3}

Ao fechar os meus olhos,
Ainda posso sentir o teu cheiro
E entre os meus dedos
Os fios dos teus cabelos.
Ainda consigo sentir a tua força
A pintar vermelhos e roxos
No meu pescoço costas coxas
Pincelando o meu corpo.
Ainda posso ouvir
O som da tua voz
E, então, diante de mim
Logo nos visualizo a sós.
E as cores que eu vejo
Ao lembrar do teu timbre
São as do arco-íris do meio
Do caminho em que contigo estive.
Durante esse devaneio
Eu mesma me aperto
Com as mãos em cheio
Pra te sentir mais perto.
Fingir que você está aqui
Pra matar a saudade
Te colando a mim
Aberta pro que me invade
Me devora me surpreende
E me faz querer te dizer
Que entre a gente
Não precisa haver
Separação
Ou distanciamento
Entre palavra e ação
Porque nos dois há sentimento.
E isso é o que mais deve importar.
Está além do dizer e fazer
E é o único que pode os demais comportar:
O sentir, este sim é o ser
Da mais profunda comunhão
Que não separa mundos
E sim os une, o real à imaginação
E os faz caminharem juntos:
Agir conforme dizemos
Dizer conforme agimos
Sem nem mais nem menos
Do que o que sentimos.
Só com o sentimento
Amamos genuinamente
E toda ação se torna um jeito
De afirmar o que se tem na mente
De fazer poesia sem papel
E construir o impalpável
Com o sentir cada poesia é um céu
Incomensurável
Constelações de sílabas
A formar uma mensagem
A constituir o belo da vida
Afinando qualquer defasagem.
E, nesse sentido,
A poesia pode ser potência e ato
E os atos podem ser poéticos -
Ambos no amor mergulhados.
