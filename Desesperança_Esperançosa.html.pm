#lang pollen
◊define-meta[title]{Desesperança Esperançosa}
◊define-meta[author]{Elis Bondim}
◊define-meta[date]{15.08.2020}
◊define-meta[audio]{Desesperana_esperanosa_-_.wav}

Queria que teu cheiro
Ficasse impregnado 
Na minha pele nos meus cabelos,
Pra que todo ar respirado 
Tivesse meu atual aroma favorito, 
A ponto de ser parte das minhas vias respiratórias  
Trazendo a alegria que sinto
Quando compartilho com você as horas,
Que eu sei que estão passando,
Sendo cada uma a mais também uma a menos
E que eu não significo tanto 
Quanto eu tento. 
Eu sei e me dói
Que não sou tão querida 
E por dentro me corrói
Toda essa vontade minha.
Mas ao mesmo tempo
Quero viver todo minuto,
Que você em contento
Me oferecer do teu mundo.
Que eu possa sentir
Teus cachos nas minhas mãos
E carregar você em mim 
Mesmo que como um profundo 'não'
Que a vida me trouxe:
Tão feliz e tão triste.
Sinal do que não me coube 
Mas hoje sei que existe. 
Sofro porque hoje não me encaixo
No teu nobre coração,
Tão bonito e tão sensato 
Em que eu não desperto tanta emoção,
Não o suficiente
Pra você se entender,
Pra fazer da gente
Mais do que eu e você:
Nós. 
Você tem os seus 
E precisa desatá-los,
E ao que parece não os dedos meus
Que poderão ajudá-lo. 
Eu choro porque lamento,
Eu lamento porque te quero,
Eu te quero porque em você eu vejo
O melhor que da vida eu espero. 
O meu elemento mágico
É o de que sei o que desejo
E o 'a dois' só me é imaginado 
Se for desse jeito.
Eu dispenso aqueles com quem não me identifico
E pra mim só vale à pena
Quem liga pro que eu ligo,
Quem também deseja 
Aquilo pelo que eu luto
E que também pensa 
Poder dividir mundos
Pra criar outro
- o nosso -
Nem sempre redondo,
Mas nunca caótico
Porque nele nos veremos
Como parte integradora e integrante
Em que estaremos 
Como dois viajantes 
Que decidem viajar juntos
E explorar a vida,
Cada canto desse mundo 
Em harmonia. 
Eu estou aqui,
Mas há outras lá fora.
Talvez outra ganhe o teu sim 
E você vá embora.
Eu ainda estou aqui 
Por ser onde eu quero estar 
E quem sabe você ainda veja em mim
E sinta e queira esse lugar 
De estar ao meu lado 
No desafio e na delícia
De um viver compartilhado,
Felicidade sentida e construída. 