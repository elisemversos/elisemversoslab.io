#lang pollen
◊define-meta[title]{Proposta}
◊define-meta[author]{Elis Bondim}
◊define-meta[date]{22.10.2020}
◊define-meta[audio]{Proposta.ogg}

Se não gostou, pode falar.
Porque eu sou toda ouvidos
Até pro que não me agradar:
É pra mudar que eu existo.
E no ato de cada mudança,
Eu aprendo eu cresço
E alimento minha própria esperança
Em ser melhor quando me deito.

Se gostou, pode falar também
Porque eu sou toda doação
E gosto de me dar a quem
Agrada-me o coração.
Eu me presenteio 
No meu ato de ser presente
A quem eu vejo
Com o que só o peito sente.
 
Se não sabe se entendeu, pode perguntar,
Verificar minha fala meus erros ou acertos
Porque eu sou toda tentativa de me expressar,
Mas falho, contra o meu próprio desejo.
Só te quero bem, meu bem, gosto de você,
Quero você bem só quero levar o bem,
Eu me importo eu falo eu corro atrás sem temer
Porque eu quero te entender.

Eu te faço perguntas 
Porque você é todo um mundo
Que me desperta pra além da mulher nua
Que já te tirou fotos quase no escuro.
Não quero te cobrar comparar mensurar,
Só te conhecer.
O que você estiver disposto a mostrar,
Da alma por trás das fotos e palavras que se vê.

Eu sou toda aberta
Porque, dentre todos os erros,
De uma coisa estou certa:
Eu não tenho medo receio,
Eu quero mais é crescer.
E só se cresce na troca,
No dar e receber
Crítica conselho resposta.

Eu sou só boas intenções
De diálogo carinho amizade,
Pra muito além das impressões
Trazidas pela virtualidade.
O real é sempre diferente,
E ele não é escrito.
Ele é o que só acontece se a gente
Vai pro âmbito do vivido.

Eu te gosto eu te aprecio
Nesse teu ar de mistério.
Não capto bem teus indícios,
Mas vou me atentar a sério
Ao que eu digo, ao que não é claro.
Sei que está sob o meu poder
Observar o que faço
E assim me refazer. 

Todo conhecer é um processo
E ele é interminável,
Pode falar, meu bem, que eu prezo
Cada ato de fala que me é direcionado.
Falemos pro nosso bem,
Pra essa troca que é o diálogo,
Que alimenta todo aquele
Que tira o melhor de todo fato.

Para P.T.