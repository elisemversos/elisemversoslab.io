#lang pollen
◊define-meta[title]{Crenças}
◊define-meta[author]{Elis Bondim}
◊define-meta[date]{12.10.2020}
◊define-meta[audio]{Crenças.ogg}

Sempre acreditei no amor 
E continuarei acreditando,
Seja lá pelo caminho que eu for 
Sei que ele irá me acompanhando. 
Mas hoje acredito também
Nessa energia sexual,
Que eu neguei até ontem 
E hoje acolho de igual pra igual. 
O que foi já foi
E é uma parte linda do que sou. 
Minha história, cada encontro 
A quem eu amei e quem me amou. 
Só que hoje tô firme e forte
Pra outras empreitadas,
Desejos de toda sorte,
Quebrando correntes já enferrujadas. 
A roupa que eu vestia 
Já não me cabe mais,
Ela me encobria 
Tanto que era demais. 
Mas eu não defendo a exposição de corpos,
Não tô aqui pra inspirar punhetas, 
Eu proponho sermos o que somos
E que naturalizemos todas as facetas
Da nossa existência nesse mundo
De alma, mas também instinto,
A aceitação de tudo 
Que é naturalmente vivido. 
Eu não busco elogios,
Mas mudanças de comportamento,
Que assumamos tudo aquilo 
Que nos faz pulsar por dentro. 
Corpos são natureza,
Minha natureza é meu corpo.
Mas é claro que minha grandeza 
Foge a todo visível escopo. 
E o que todos vêem é superfície
De toda uma profundeza,
Que, por mais que a outros atice,
É fogo de palha que não sustenta.
Porque o que dá base 
A mim mesma e a qualquer 'nós'
É troca diálogo sentimento que não se acabe 
Em fugazes momentos a sós. 
A diferença não está
Em que as chances de amor não mais existam,
Mas apenas que agora também há
Outras energias que me abrigam. 
Anda tudo lado a lado,
O que por tanto tempo 
Caminhou em separado 
Na história que ainda tô escrevendo. 
