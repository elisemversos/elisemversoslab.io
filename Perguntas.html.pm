#lang pollen
◊define-meta[title]{Perguntas}
◊define-meta[author]{Elis Bondim}
◊define-meta[date]{28.06.2020}
◊define-meta[audio]{Perguntas.wav}

Eu ameaço ir embora 
Pra ver se te convenço
A me agarrar sem demora
E de um jeito mais intenso 
Mas de nada adianta
Eu não tenho esse poder
E fico que nem criança
Batendo o pé a se enfurecer.
A teimosa encontrou o teimoso
Dois persistentes em seus valores
Caramba, mas que encontro!
Dois arriscando em seus amores!
Talvez pela nossa intransigência
Estejamos perdendo uma estória
Imprevisível, mas que em potência
Parecia ser no mínimo bem gostosa!
Talvez sejamos os dois 
Menos inteligentes do que gostaríamos?
Porque de que vale não esperar o depois
Se só nele estiver o que viveríamos?
Ou de que vale não viver o hoje
Se nele estiverem os melhores momentos?
Como seria se um de nós fosse
Um pouco mais forte na arte do convencimento?
Ou se fôssemos menos rígidos
E um pouco menos insistentes
Na insistência dos nossos ímpetos
Pra que não abríssemos mão da gente? 
Não temos essa resposta.
Só temos perguntas
E o agora bate à porta
Deixando somente dúvidas.