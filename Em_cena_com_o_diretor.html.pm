#lang pollen
◊define-meta[title]{Em cena com o diretor}
◊define-meta[author]{Elis Bondim}
◊define-meta[date]{24/09/2020}
◊define-meta[audio]{Pro_cara_do_cinema.ogg}

Se você tem receio em contar teus desejos
Vou eu então te contar os meus:
Eu te daria muitos beijos,
Ansiando pelas tuas mãos sobre os meus seios 
Até transpirarmos tanto
Que nos tirarmos a roupa seria irresistível.
E, com a nossas peles se encontrando,
Seria pra você visível
Aquilo que não se vê nas fotos.
E quem sabe até você tiraria uma
Só pra depois ter diante dos teus olhos
Uma mulher, além de nua,
Olhando pra você de volta
E querendo essas tuas tattoos,
Suas cores e formas,
Que, junto ao teu corpo também nu,
Se confundiriam com a minha pele
Colocando-se entre minhas pernas,
Braços, cabelos, mãos que te querem
A me explorar com a liberdade plena
De quem teria recebido o aval 
Pra estar sob, sobre, acima, abaixo
E curtir o ato carnal
Com a alegria de satisfazer o desejado.
E eu bem ia gostar 
De tocar cada linha colorida 
Desse teu corpo a habitar
Minha mente imaginativa.
Esse teu corpo de cinema
Que eu desejo hoje,
Só porque toda a beleza
Tá no sentido não do que fosse,
Será ou poderia ser,
Mas do que eu crio
Ao olhar pra você
Quase como gata no cio,
Mas que se atiça com os sentidos
Que eu mesma engendro
Ao querer esse teu brinco 
Sendo meu brinquedo;
Essa tua barba e tua língua
Prazerosamente se entretendo 
Nos meus bicos pelos curvas
Até que a gente, se vencendo,
Precisasse concluir
Pela frente por trás,
Do jeito que estivermos a fim,
Aquilo que estar aqui nos faz.
Aqui na minha imaginação
A te querer numa quinta-feira,
Só porque gosto de pensar a ação,
Que pode nunca ser verdadeira.
Mas que na minha mente
Me faz querer teu enquadramento
A transformar a gente
Numa cena gostosa de quem, vivendo,
Se encontra pra se querer, 
E se quer pra se encontrar
Porque ah, esses olhos que você
Usa pra filmar,
Eu os quero sobre meu corpo
Me recortando ao teu bel-prazer,
Porque no teu próprio gozo
Eu encontro hoje o meu querer.