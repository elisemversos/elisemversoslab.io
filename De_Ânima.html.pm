#lang pollen

◊define-meta[title]{De Ânima}
◊define-meta[author]{Elis Bondim}
◊define-meta[date]{14.08.2020}
◊define-meta[audio]{De_Ânima.wav}

De tanto eu falar em paixão,
O menino agora quer se apaixonar.
Só que cada coração
Tem o seu jeito de amar. 
É verdade que penso 
Que as quedas elevam o vôo
E trazem belo e imenso
Encanto forte e animador. 
Mas o que é o ânimo
Que não a alma de cada um?
E, pra uma alma calmo oceano,
Talvez nova onda não faça "boom",
Porque o amor nem sempre faz clique,
E às vezes demanda tempo, 
A gente não quer tanto que fique,
Mas aos poucos surge com o vento,
Nem ventania nem tempestade,
Nem terremoto nem maremoto,
Nem ciclone que invade 
E faz tremer os corpos.
Há amores que são brisas suaves 
E há almas que são serenas,
E não há quem ame mais;
Diferente, apenas. 
Alguns se fascinam tanto 
E sentem arrepiar os pêlos,
Porém contudo no entanto 
Um tanto quanto no atropelo. 
Outros precisam de meses 
Ou até mesmo de anos, 
Pra um dia pensarem que sentem 
Algo forte que dá ânimo. 
Ânimo em estar junto,
Ânimo em ir pra cama,
Que nem sempre nos deixa surdo,
Que nem sempre nos inflama,
Ou nos cega e nos desequilibra,
Nem sempre nos deixa ansiosos,
Mas com o tempo se fortifica 
E tranquiliza aos receosos.
Não se martirize, meu bem,
Se você não sentir à flor da pele. 
É que ninguém é igual a ninguém
E às vezes tola pessoa se fere 
Por não compreender que o outro
Não pode dar o que não tem 
E que às vezes não é o encontro 
Que traz isso a alguém,
Mas outros fatores,
Dentre os tantos que existem,
Que transformam em amores
O sutil latente ou o que quer que as pessoas sentem.
Descobrir, descobrir,
Cada um a seu modo,
O que está muito além de mim 
E pro que sou só um passo módico.
Mas que talvez possa ser 
Dos primeiros de uma caminhada,
Em que quero estar com você,
Ainda que não como amada.
Por isso colo minhas lágrimas
No tecido da roupa que me veste, 
Mas engulo até as mais amargas
Buscando ser maior do que o breve 
Temporário fugaz e volátil 
Desejo de que tua pele 
Em mim fosse naufrágio
E se deixasse ficar 
Por tempo indeterminado,
Dando o benefício de duvidar 
De que o que há talvez seja barco 
Que leve a outros mares, 
Mas de algum modo companheiro
Nessa longa viagem 
Que é navegar em velejo. 
O risco de se afogar 
É risco assumido
Ao se decidir navegar
Sem porto conhecido.
Mas não se preocupe, meu bem,
Eu nado no fluxo da emoção,
E pode até não te parecer, 
Mas eu nado bem até na contramão,
Levando-me pela corrente sentimental 
Que me percorre inteira 
E me leva ao paraíso e à profundeza abissal,
Sempre eu, sempre intensa. 