#lang pollen
◊define-meta[title]{Minha experiência como mulher}
◊define-meta[author]{Elis Bondim}
◊define-meta[date]{09.03.2020}

Eu não posso andar na rua
Com tranquilidade.
Mesmo vestida sinto-me nua
E sem pudor me olham com voracidade.

Eu não gozo
Com facilidade. 
E não me toco
Com naturalidade.

Eu já fiz sexo
Sem vontade.
Não há nexo,
Veleidade. 

Eu cubro meus seios
Porque já foram tocados 
Sem permissão, sem direito
Assediados. 

Eu não uso biquíni
Porque não tenho corpo padrão.
Disseram que era feio como crime
Mostrar ao mundo minhas curvas como são. 

Há dias em que estou mais carente
- o nome disso é TPM.
Este fenômeno, nenhum homem o entende,
Mas vários o temem.

Há dias em que sangro
- o nome é menstruação.
Nenhum homem, preto ou branco,
Tem disso nem sequer noção.

Eu sou menina, sou mulher
Não tua, mas como canta Cássia 
- minha, não de quem quiser. 
Achar o contrário é muita audácia. 

Não tente me controlar.
Você não está na minha pele.
Todo ato que queira me regular
É motivo pra que eu me rebele.  

Ser mulher é ser forte
Sem ter outra opção.
Toda fragilidade que te choque
É sinal da nossa potência de renovação. 

Todo dia a gente acorda
Vai pro trabalho e pro tanque.
E não há macho que bata à nossa porta
Que com facilidade o mesmo aguente. 

Tomem cuidado,
Prestem atenção.
Se vocês não têm tato
Nós aprendemos a dizer: - NÃO! 