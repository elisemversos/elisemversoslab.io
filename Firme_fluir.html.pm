#lang pollen
◊define-meta[title]{Firme fluir}
◊define-meta[author]{Elis Bondim}
◊define-meta[date]{25.06.2020}
◊define-meta[audio]{Firme_fluir.wav}


Gosto de cada coisa no seu lugar,
Claras como a luz do dia.
E sei que nem adianta eu tentar nadar
Contra a mais forte corrente marinha,
Que é a do meu próprio ser,
Que me constitui assim,
Tão fluida quanto firme
Quando sei o que carrego em mim.
Sou como pólen que se espalha
E fecunda em vários jardins,
E voa solto pelo ar,
Levando ao mundo um pouco de si.
Chego fácil, vou-me mais fácil ainda.
Digo adeus como quem diz oi
E me despeço em lágrima,
Que, quando eu vejo, já se foi.
Escorre na hora do banho,
Debaixo da água quente,
Durante a TPM em que estou
E quando observo o sol poente.
Eu me desprendo
Do mesmo modo como agarro,
Eu luto pelo que quero
Mas também sei o quanto valho.
Eu quero o saudável,
Mas tenho minhas doenças.
Eu não sou cura nem curável,
Sou criança e suas proezas.
Eu rio na cara do perigo
E morro de medo de altura.
Ainda assim o risco é meu amigo
E eu até perco a compostura
Porque me dou sem pensar antes
E sem prever as consequências.
E aí me dilacero com o dissonante
E seus sons agudos em ascendência.
Eu estou sempre à beira do precipício
Dançando com as sombras,
Desnudando o inimigo
E me afogando nas ondas.
Eu gosto do novo
E gosto do fixo
Um contraponto
E eu toda me contradigo.
Sou toda opostos,
Oito ou oitenta,
Intensa como um terremoto
Que abala o planeta.
Sou tudo ou nada,
Cuspo na cara do pouco,
Posso ser agressiva e mimada
E sei o que busco.
Quero menos sentimentos
Pra conseguir ser mais equilibrada,
Mas como abrir mão do que tenho
De tão único e belo na minha alma?
Se o que eu sou de especial
É justamente essa capacidade
De subir ao céu e descer ao umbral
De ser eletricidade
Que ilumina e que dá choque,
Que é vida e fio encapado,
Que ressuscita e mata em um toque,
E deixa tudo ligado.
Eu me nego e me afirmo
Todos os dias
Sou montanha russa que não desligo,
Mas sou minha e só minha.