#lang pollen
◊define-meta[title]{Padrão}
◊define-meta[author]{Elis Bondim}

Dizem que só somos bonitas
Se formos magras.
Dizem que só somos magras
Se pesarmos 50.
Limitam as formas infinitas
E querem nos tornar fracas,
Tirar nossa graça,
Ver até onde a gente aguenta.

Quantas dietas loucas
Nosso corpo suporta?
Quantos remédios,
Venenos tóxicos?
Não são poucas
As almas mortas
Por falta de intermédio
De um mundo mais sóbrio. 

Dizem que só somos sexy
Se formos fitness. 
E que só é bela a nudez
Dos estereótipos. 
Não nos enxergam mais
Não vêem que isso é illness.
Sofrem de surdez
Desnaturalizam biótipos.

Mas estamos aqui.
Firmes e fortes,
Pra lutar contra a opressão
Dos nossos corpos.
Dizendo sim
E sendo suporte
À nossa libertação
De todos os modos. 