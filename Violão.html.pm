#lang pollen
◊define-meta[title]{Violão}
◊define-meta[author]{Elis Bondim}
◊define-meta[date]{Janeiro 2020}
◊define-meta[audio]{Violão.ogg}
Quero que tu me toques
Como tocas teu violão,
Harmoniosos acordes
Da mais bela composição.

As cordas geram sons,
Os toques tocam os espíritos
E surgem diferentes tons,
Por mim antes inauditos.

Quero que teu lirismo
Se torne ato 
E todo o romantismo
Seja a realidade do tato.

Transpiração que escorre
No corpo dessa música,
Desejo de que em mim more
Essa sensação tão única.

Quero que tu escrevas
Uma letra para mim
E que tu me sejas
Inspiração sem fim.

E assim o querer vira poesia
E tu compões outro arranjo,
Enquanto minh'alma me irradia,
Já que eu não me abranjo.