#lang pollen

◊define-meta[title]{Abertura}
◊define-meta[author]{Elis Bondim}
◊define-meta[date]{2019}

Há mudanças que vêm para o bem
E se pro bem vier é bem-vinda.
Se a gente quer, o que é que tem?
Essa é a nossa vida.

Se só se vive uma vez,
Então que a vida seja mais.
Que não haja talvez,
Muito menos jamais.

Que a gente possa estar junto
E também olhar sem culpa.
Que a gente possa tudo
Sem precisar de desculpa.

Que cada arrepio seja um convite
Pras delícias da vida,
Sem regra que dite
Como ela deve ser vivida.

Não serão poucos
Desejando que a gente mude.
Mas se os outros são os outros
Eles que se acostumem.

Que viver seja intensificar.
Somar e dividir,
Não se privar
Das possibilidades sem fim.

Que cada desejo
Não tenha de ser contido.
Que seja lampejo
Que impulsiona ao desconhecido.

Que cada toque
Seja sentido
Impacto, quase choque
Libido.

Que viver seja viver
O que dilata a pupila.
Liberdade de ser
Sem grila.

Que a gente jogue fora
Tudo que não nos serve.
A vida é o agora,
Não o enerve.

Juntos somos melhores
E livres somos completos.
Sei que somos maiores,
Estamos no caminho certo.
