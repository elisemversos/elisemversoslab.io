#lang pollen
◊define-meta[title]{Manhã de um domingo}
◊define-meta[author]{Elis Bondim}
◊define-meta[date]{28.06.2020}
◊define-meta[audio]{Manhã_de_um_domingo__.wav}

Acordo e penso em você
E, ainda deitada na cama,
De tudo que eu queria fazer
O que eu mais queria é te beijar em chamas
Chamas que rapidamente se espalhassem 
Pelo teu rosto e teu corpo.
Não há jeito, essa é a imagem
Cuja falta me tem gosto insosso!
Meus lábios sobre a tua pele,
Visualizo e me dá água na boca.
A ausência é um gelo que me fere
E te ver sem isso me deixaria louca. 
Juro que eu queria conseguir
Que não fosse esse o gesto 
Que vem de dentro de mim 
E não tem nem a ver com sexo. 
Dele eu até abriria mão,
Por mais que também o queira.
Mas do beijar-te a face não consigo não,
Pois é o movimento físico que me incendeia!
Incendeia a alma de quente afeto
E ao corpo de vontade do teu calor.
A falta dele tem efeito indigesto 
E o sonho é o que nos leva aonde vou - 
Leva-nos dentro do meu peito 
E me aquece os dias 
Até se apagar por completo
Ou você aceitar colar na minha.