#lang pollen
◊define-meta[title]{De 17 a 22}
◊define-meta[author]{Elis Bondim}
◊define-meta[date]{23.03.2020}
(Para B.)

De 17 a 22
Do mês de março de 2020
O agora e não o depois
Invadiu minh'alma e mente.
No primeiro dia
Eu fui misto de receio
Com a adrenalina
De quem enfrenta um medo.
No segundo dia,
Eu fui renovação
Com a alegria
Da paz da reflexão.
No terceiro dia,
Eu já abracei o momento
Sentindo a euforia
Do autoconhecimento.
No quarto dia
Eu já fui paixão,
Tentando estar em sintonia
Com o meu corpo em explosão.
No quinto dia
Eu já estava encantada
Com as loucuras da vida
E suas novidades inesperadas.
Na última noite,
Eu já não queria o amanhecer
E seja lá o que a vida me apronte,
Estou pronta pra viver.
No último dia,
Eu já era saudade
Sorriso de quem desconfia
Mas feliz pela oportunidade.
O mundo em quarentena
E eu em descoberta;
Reclusão que concatena
Quem eu sou e quem eu era.
Lidamos todos os dias
Com o extraordinário
Se olharmos com sabedoria
Pra cada detalhe do nosso itinerário.
Hoje já é outro dia
E eu por inteiro
Já sou toda nostalgia,
Sozinha neste apartamento.
O que é estar em paz?
O que é estar feliz?
É estar assim, sem mais,
Nada além de mim.
Não sei se é presente
Tampouco se é futuro.
Mas já estou contente
Por cada segundo.
Memória
Motriz;
Estória
Feliz.
