#lang pollen
◊define-meta[title]{Trabalho poético}
◊define-meta[author]{Elis Bondim}
◊define-meta[date]{30.05.2020}
◊define-meta[audio]{Trabalho_poético.mp3}


Às vezes o trabalho me chama
E a escrita fica um pouco de lado,
Mas a poesia sempre me alcança
Em cada olhar projetado.
Ela é mais do que as palavras:
É um modo de viver
Que rima com as árvores
E eu caso com o que os meus olhos veem.
Ela constrói frases soltas
Com o azul que me preenche,
O brilho estelar que me assoma
E o gosto matutino do café quente.
Ela me aparece quase sorrateira
Quando fecho os olhos e sinto
O sorriso que me invade inteira
Simplesmente porque vivo.
E desse jeito de ser em poiesis
Vou me constituindo
Como Elis 100% aisthêsis
Que se cria ao outro ir construindo.
E nesse fluxo toda a minha história
É, com o meio, troca da mais pura
Em versos está escrita toda a minha memória
E esse estilo me é cura,
Na qual ofereço meus seios como poesia erótica
Assim como a fotografia da minha pele nua;
Mas que podem ser porta de entrada a outra ótica
Que me enxergue pra além da minha carne crua
Porque se as curvas do meu corpo
São como versos livres
Também o é todo o frágil contorno
Que marca tudo que minha existência diz.
Algumas lembranças são quase heroicas
Sobre as quais faço questão de não mais ser muda;
Alguns abusos são poesia sórdida
Que quase me prendem numa câmara escura.
Alguns percalços são lamentos de injustiça
Mas a superação faz versos fortes de renovação;
Já as pessoas são poema na forma de música
E letras inapagáveis no coração.
Os amores são poesia romântica
E as aventuras trazem adrenalina;
E um gosto doce de esperança
Trazem os poemas de menina.
É assim que eu me dou ao mundo:
Numa forma poética
Cujos poemas mais profundos
Dão à minha vida uma forma estética.

 

 
