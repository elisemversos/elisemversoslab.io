#lang pollen
◊define-meta[title]{No país do carnaval...}
◊define-meta[author]{Elis Bondim}
◊define-meta[date]{18.05.2020}

 Quantas pessoas ainda ficam chocadas
Por uma mulher expressar quem ela é
E me olham assustadas 
Quase tremendo sobre os próprios pés
Porque fomos ensinadas a nos calar 
E rejeitar parte de nós mesmas 
Em nome de preservar - é importante que ninguém se esqueça - 
Um moralismo hipócrita,
Um machismo opressor,
Uma visão torta
Sobre o sexo e o amor
Que nega a própria vida 
E o que há de mais natural
Que ao mesmo tempo nos idealiza
E coloca nossa sexualidade num pedestal. 
Muitos dizem: cale-se
E eu digo a todos: pois então eu grito! 
Caso não saibam, pasmem:
Eu existo! 
Meu desejo também existe.
E não vou mais fingir que não.
Porque minha força insiste
Em ser mais forte que a repressão.
Eu vou falar, ao fotografar e escrever, 
Respeitando os meus limites,
Mas não o alheio querer
Que pede que a mim mesma eu evite. 
Você que se retire 
E a todo o seu preconceito.
Ou que fique 
Junto e sem receio.
Sem transformar em tabu 
A energia que é a mais vital 
Respeitando meu corpo nu,
Minhas palavras e meu carnaval
No qual as máscaras são só um pretexto 
Pro brincarmos em diferentes papeis 
E as fantasias trazem contextos
Coloridos que nem confetes. 
Uma dança leve e alegre
Na qual assumimos quem somos
Sem espaço pra bobagens 
Como julgamentos tolos. 
Eu vou utilizar todos os meios
Que me forem possíveis 
Pra mergulhar sem medo
No processo de nos tornar visíveis.
Naturalizar o que por tanto tempo
Eu mesma neguei em mim 
Porque hoje eu entendo 
Que a vida é mais genuína assim.
Sei que é uma luta de gigantes
Mas eu não temo os cortes.
Sei que pode vir o sangue,
Mas também sei que eu sou forte. 👊🏻
