#lang pollen
◊define-meta[title]{Quero-te}
◊define-meta[author]{Elis Bondim}
◊define-meta[date]{20.09.2020}
◊define-meta[audio]{Poema_Quero-te.ogg}

Quero-te ser sol 
Pra te iluminar.
Não porque só
Você não seja ser a se bastar.
Mas porque somos dois sois
Cujas luzes podemos trocar,
Colorindo mais nossos arrebóis 
E o céu em que podemos nos encontrar. 

Quero-te ser chuva 
Pra te lavar a alma
E tua pele nua,
Em que quero com calma 
Me escorrer gota por gota 
Pra matar tua sede,
Pro que quem sabe eu toda
Possa um dia ser suficiente. 

Quero-te ser brisa 
Pra refrescar tuas noites,
Trazendo frio que arrepia
No momento em que encontre 
Teu pescoço nuca costas,
Que às vezes quero tocar
Sutil como quem encosta 
Tentando acreditar.  

Quero-te ser ar 
Pra te dar mais fôlego,
Ainda que tirar 
Também seja do meu gosto.
Mas oxigênio faz viver 
E nos dá cada novo dia, 
Pra gente buscar ser,
Respirando sob outra perspectiva. 

Quero-te ser aroma 
De flor esbelta a enfeitar 
O jardim em que se encontra
Tua alma bela a plainar,
Assim como que atice
Teu desejo por mulher;
E quem sabe ainda existe 
Chance de ser o que não é.  

Quero-te ser tempo 
Pra haver resposta à esperança,
Vivendo a contento
Em nossas andanças,
Em que podemos nos perder,
Mas também nos achar.
Talvez nos escolher,
Talvez no passado nos colocar. 

Quero-te ser carinho 
Porque quando eu te olho 
É isso o que eu mais sinto 
Saindo pelos meus poros. 
E a melhor parte de estar com você,
Contra muito do que já foi,
É apenas estar a te ter 
Por perto, de algum modo e onde for. 

Quero-te ser aceitação
Pra que eu possa ser também
Fonte que jorra com o coração
O melhor possível a outro alguém. 
Fazendo-me paciente
Pra viver em outro ritmo, 
O único que entre a gente
Nos dá um caminho. 

Quero-te ser 
O que eu puder ser,
Quem sabe melhor te conhecer
Talvez até melhor te entender. 
Sou eu mesma,
Não sei ser de outro modo. 
Sou mesmo aquela que tenta
Porque eu me importo. 

Quero-te 
Sendo o que quiser ser,
Certo de que
Eu adoro você. 
E que se eu te quero 
É porque te vejo 
Com um brilho que eu prezo
- fruto do teu jeito. 

Quero-te ser o indefinido
Mas um indefinido feliz,
Num tempo também impreciso 
Sem amanhã ou raiz. 
E que a vida se abra 
Instaurando possibilidades, 
As quais nunca acabam 
E nos uniu em imprevisibilidade. 
