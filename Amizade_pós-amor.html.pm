#lang pollen
◊define-meta[title]{Amizade pós-amor}
◊define-meta[author]{Elis Bondim}
◊define-meta[date]{11.04.2020}
◊define-meta[audio]{amizade_pós-amor_1.mp3}

Talvez ainda criemos juntos
Os nossos filhos 
Sem, contudo, 
A vida que já dividimos. 
Talvez eles brinquem na praça
E nos façamos visitas dominicais,
Mas cada um com a sua casa,
Na felicidade de outros casais. 
Um novo amor já vem 
E não tarda a brilhar;
E a amizade que a gente tem 
Não precisa se quebrar. 
Ele está logo ali na esquina 
E nós estamos na história;
Que abracemos o que se aproxima, 
Contentes com a nossa memória. 
O que foi não se repete 
E isso faz parte da graça
De viver bem e da arte 
De transformar tudo que passa. 
Talvez a gente ainda divida
A mesma mesa num seminário 
A presença em fotografias 
E celebrações de aniversário;
Uma garrafa de vinho,
E quem sabe não saiamos a quatro! 
Os gatos os vôos os livros
E jogos de baralho.
Conversas sobre Oswald,
Dicas de filmes,
A cerveja num balde 
E torcer pro mesmo time. 
Ou talvez sigamos outros rumos
E sejamos passado feliz,
Com o sentimento mais puro 
De termos vivido até o fim. 
A amizade é um carinho 
Que o tempo e a distância não rompem  
É um querer bem infinito 
Independente do que tombe. 
Porque sabemos que no caminho 
Muito tombou e se desfez 
E continuamos vivos 
Abertos pra outro alguém outra vez. 
Uma nova ordem já se impôs 
E outros eus assim se instauram;
E aquilo que agora somos 
Ainda é belo em sua aura. 
Acho que a amizade é fruto 
De qualquer amor sadio.
E é a ela que aludo,
É ela que irradio. 
