#lang pollen
◊define-meta[title]{Presente}
◊define-meta[author]{Elis Bondim}
◊define-meta[date]{Abril 2020}
◊define-meta[audio]{presente__.mp3}

(Para B.)

Eu vou me embrulhar 
Com papel de presente,
Só pra te dar 
Meu corpo alma e mente.
Vou usar um papel ouro
E um laço grená,
Pra parecer um tesouro
Que atraia teu olhar.
Porque cada camada de mim
Quer te ser revelada,
Desde minha boca em carmim
Até minha energia mais elevada.

Você pode usar tuas mãos,
Tua boca teus dentes,
Pra rasgar cada canto 
Conforme te convém
Da camada epidérmica,
Que é a base do sexo,
Da beleza estética 
E do gozo mais sincero. 
Aqui eu me ofereço 
Ao teu tato teu paladar 
A tua força e teu aperto 
A cada dedo teu a me tocar.
Essa troca controla a perda d’água
Do suor dos nossos poros,
Regula a temperatura 
No choque dos nossos corpos. 

Depois aderme já não é superfície da pele,
É metáfora pro imaterial:
É pensamento que se desvele,
Onde a palavra é o canal. 
Representa a elasticidade,
Colágeno da psique,
E toda a capacidade 
De transformar o que existe. 
E aqui tuas mãos já não são úteis;
Só o dialogo troca e cadência -
Como um livro que se estude,
Exige atenção zelo e presença. 
Essa é a camada mental 
De conexão pelo pensamento,
Onde se encara de igual pra igual
Toda uma linha de sonho plano e tempo. 
Aqui você precisa de mais pra me abrir
Do que os cortes provocados pelas tuas unhas:
Precisa ser o que diz 
E me deixar ouvir cada fala tua. 

Já a camada de mais difícil acesso 
- Hipoderme transcendental -
Só se adentra com muito esmero,
Só com uma força colossal.
Aqui é o local de reserva de energia 
Preservada pro que for importante, 
Defende contra pancadas físicas 
E não deixa que o fluxo se estanque. 
Aqui só o teu eu mais íntimo
Pode me desembrulhar,
Encontrar o caminho 
Do laço a desamarrar. 
Essa é a camada da alma,
De doação espiritual,
Onde a luz não falta 
E está o meu eu sem pedestal. 
Humilde em sua existência
E belo em sua coragem
De se despir de toda a aparência
A outra alma que lhe invade. 

O papel ouro 
E o laço grená
São só sinal do puro
Convite a te chegar. 