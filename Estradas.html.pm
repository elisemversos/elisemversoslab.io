#lang pollen
◊define-meta[title]{Estradas}
◊define-meta[author]{Elis Bondim}
◊define-meta[date]{22.09.2020}
◊define-meta[audio]{Poema_Estradas.ogg}

Somos dois estranhos
Que se depararam
Na mesma estrada,
De que extraem
O que podem e decidem.

Quatro olhos castanhos
Que se olharam
Na mesma estrada
Em que, a duvidarem,
Vivem as inseguranças que lhes incidem.

Dois seres que, se encontrando,
Quase se separam
Na mesma estrada
Em que, ao se esbarrarem, 
Vivem o que se permitirem.

Dos corpos que, se encostando,
Se desejam e se tocam
Na mesma estrada,
Sobre o mesmo estrado
Descobrindo o que sentirem.

Duas almas que, voando,
Conversam ou se chocam
Na mesma estrada
Em que, sem momento exato,
Talvez não mais caminhem.

Duas pessoas que, no entanto,
De algum modo se gostam,
Na mesma estrada
Em que, lado a lado,
Ainda insistem. 

Duas interrogações que, perguntando,
Buscam respostas
Na mesma estrada
Em que, separados,
Não existem.

Porque o nós, uma vez não estando
A compartilhar as horas
Na mesma estrada
Em que se está como entes individualizados,
Ele não subsiste.

Mas ainda somos dois, que conversando,
Buscam o ponto em que se reencontram,
Na mesma estrada
Quem sabe extrapolar o estipulado
Como duas vidas que realmente vivem. 
