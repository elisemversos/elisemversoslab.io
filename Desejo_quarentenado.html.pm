#lang pollen

◊define-meta[title]{Desejo quarentenado}
◊define-meta[author]{Elis Bondim}
◊define-meta[date]{14.05.2020}
◊define-meta[audio]{Desejo_quarentenado.mp3}

(Para B.)
Pela primeira vez
Até que eu me imagino,
Apesar da timidez,
Diante de uma câmera me despindo.

Queria deixar o meu vestido cair
Pra depois também cair a roupa de baixo
E te imaginar diante de mim
De todos os modos em que me encaixo.

Sermos peças diversas de um quebra-cabeça
Ou de um tetris físico e sensual
Em que cada peça que se mexa
Constitua nova possibilidade sexual.

Porque de fato cada parte do corpo é peça
De um jogo erótico e prazeroso
No qual não há placar que se meça
Que não seja o da comunhão do gozo.

Anseio pelo contato táctil
Não porque o isolamento me é duro
Mas porque te desejo fácil
Na cama na rua no claro no escuro.

Mas não se engane, meu bem, isso é bem pouco
Diante de todo o sentimento meu.
Escrevo muito para o teu corpo
Porque o meu pede pelo teu,

Mas meu querer-te é só uma face
Do todo que eu sou
Não há aqui nenhum impasse
Entre pele e interior.

Lá dentro há muitos quereres
Precipitados ingênuos intensos
Direcionados a você
E ao que vier com o tempo.

Aos sins e nãos do futuro
Eu me afirmo todo dia.
Sei que um poema não é muito
Mas eu o te dou como alma minha
 
Pra você guardar essa memória
De alguém que pensa em ti
Lembrança de uma estória
Que às vezes nem me deixa dormir.

Parece que são só palavras
Mas são pedaços de um mundo
No qual sinto tudo que me caiba
E te ofereço o meu gostar mais puro.

Frases soltas num papel
Ou numa tela digital
São só como um véu
A cobrir um sentir torrencial.

Não se preocupe, não sou tão boba
De superestimar o que foi vivido
Não sou pessoa que se afoba
Em se iludir com o sentido.

Mas gosto de quase levantar voo
Deixa viajar a mente
Manter só um dos pés no chão
E abraçar o que o meu eu sente.

Por isso mesmo eu te ofereço
E persisto em te dar
Esse pouco que escrevo
Mas que é todo teu e assim será.

Pois um poema uma vez escrito,
Se é verdade que cada um o lê à sua maneira,
Também é que terá sempre sido
Para quem no momento da escrita se queira.

 14.05.2020
