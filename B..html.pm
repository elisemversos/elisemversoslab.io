#lang pollen
◊define-meta[title]{B.}
◊define-meta[author]{Elis Bondim}
◊define-meta[date]{Junho 2020}
◊define-meta[audio]{B..mp3}


Eu não preciso da tua presença,
Eu simplesmente a prefiro.
E hoje mais ainda me toca a ausência,
O não poder estar contigo. 
Isso não é agressivo
E sim o possível mais sincero:
Dizer que não te necessito,
E sim te escolho e te quero.
Essa é a declaração mais forte
Que eu posso te oferecer,
Consciente de que foi lançada a sorte 
E de que em precipitação posso incorrer.
Mas é nítido que hoje, se eu pudesse,
Sem dúvidas optaria 
Ir aonde você quisesse
E ser tua companhia.
Estar junto num abraço
Seria o maior acalento;
E à distância de um passo,
Eu verbalizaria meu pensamento:
Eu te diria através do som que minha boca emite
O quanto estaria feliz por estar presente,
Se a vida assim nos permitisse
E não houvesse obstáculo, entrementes.
Nem que fosse por cinco minutos,
Gostaria de ocupar o mesmo ambiente,
Pra celebrar em sorrisos,
Essa condição básica do encontro entre a gente
Que é o ato de nascer 
E a cada ano reafirmar a própria existência
Nesse trajeto do viver 
No qual é preciso persistência.
Eu te agradeceria
Por ter se aberto a mim 
Mesmo sem poder saber como seria:
Alegre ou feliz, bom ou ruim.
E, sobretudo, eu te daria todo o meu carinho 
Pra te fazer se sentir especial
Porque é isso o que eu sinto
E queria demonstrar de modo real.
Pra além das palavras,
Eu te daria meus gestos mais doces 
E se desse ainda aproveitava
Pra fazer de mim flores
Que você pudesse tocar,
Cujo aroma te agradasse
Cujas cores você gostasse de olhar
E até do gosto provasse. 
E assim a festa seria completa,
Uma boa comemoração
Com a dose certa
De afeto e de paixão. 
