#lang pollen
◊define-meta[title]{3 meses}
◊define-meta[author]{Elis Bondim}
◊define-meta[date]{26.06.2020}
◊define-meta[audio]{3_meses__1_.wav}

Agora eu já me esqueci do teu cheiro
E já não consigo imaginar teu toque. 
Já não sinto a ânsia pelos teus dedos,
Não importa o quanto eu me esforce.
Já não lembro a textura dos teus cachos 
E as lembranças são muito distantes.
Meu corpo não sente mais o teu abraço
E já não sinto que haja chances.
Por mais que eu lute em imaginar,
Sinto que se esvai esse caso.
E a impotência me tira o ar
E desfaz toda esperança de laço.
Não há base, não há consistência
Em breves e fugazes dias,
Que estruture sustente mantenha 
Um sopro tão longo de afeto e de vida. 
Toda estória se torna passado 
Quando não atualizada,
Quando o presente não traz novos dados
E no hoje não se vive mais nada. 
A internet até aproxima,
Mas só por um curto tempo. 
Ela me traz um vazio existencial que lancina
E me leva aos meus buracos mais extremos. 
Eu já não posso suportar
Ser reduzida à não existência 
De ser imagem em tela de celular
- uma mera aparência. 
Uma visão pictórica que já não me soma;
Ao contrário, me diminui.
O nada me toma,
Me domina e me conduz. 
Então parece ser o fim
Porque tanto a paixão quanto a sensatez
Podem dividir 
Quando são umbelicais rainhas da vez. 
Ainda que o mundo esteja se acabando,
O encontro entre os nossos também está. 
O contágio viral está se espalhando 
E vai continuar. 
Destrói vidas amores esperanças,
O emocional e o psicológico.
Deixa o medo e a distância como herança 
E extermina o que podia ser nosso.
Não é culpa nossa.
Eu só não sou tão racional,
E, misturado nessa troca,
Você não é tão emocional.
Não tenho mais tempo a oferecer,
Não sou e nunca fui calmaria,
E você não tem um intenso querer
Tão forte que a razão sobrepujaria. 
De mãos atadas,
Eu lamento pela ausência.
Jogo na mesa todas as cartas
E sofro nessa contingência. 
É com muita dor que eu me sinto
Extremamente pequenina.
E, infelizmente, necessito
De mais do que mensagens escritas. 
 É com pesar que não tenho mais forças
Pra sustentar minha não concretude,
E só me resta me agarrar às pessoas
Que, como eu, cedam um pouco mais ao impulso. 
Porque está difícil de ser 
Ao mesmo tempo irreal, virtual e amante.
Eu preciso viver,
Ainda que de modo errante. 
Assim, eu sofro e me entristeço
E não vejo outro caminho.
Eu quase me despeço porque não te vejo 
E tampouco te quero só amigo.
Eu penso em te dizer adeus 
Porque te admiro,
Porém estou nesse limite meu,
Que já foi arduamente estendido.