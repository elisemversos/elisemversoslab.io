#lang pollen
◊define-meta[title]{Botões}
◊define-meta[author]{Elis Bondim}
◊define-meta[date]{Março 2020}
◊define-meta[audio]{Botões.wav}

Essa tua camisa de botões,
Eu a abriria todinha.
Essa camisa que expõe
Tua pele moreninha
Com teus pêlos pretos
Que eu quero percorrer
Com minha língua e meus dedos
Desbravando você.
Eu arrancaria essa tua camisa
Junto com a tua cara séria,
Jogaria-as pra cima
E veria feliz como seria
Teu jeito teu sorriso
Em outro contexto.
O contexto do tato
Do instinto, do inexato,
Em que a razão
Não vale de nada.
E eu queria olhar bem nos teus olhos
E beijar essa tua boca,
Transpirar com os teus poros
E ver se você estanca
Esse meu desejo doido pelo teu umbigo,
O qual eu me imagino
Ultrapassando
Com meus lábios molhados.
Tuas mãos eu as queria
Nos meus seios
E que você abrisse
O fecho éclair do meu vestido
Bem devagar
Apreciando cada centímetro
Da minha pele branca.
Bem que eu queria
Que você me quisesse
Uma centelha do quanto eu te quero.
Solto faíscas,
Não sei como me comportar.
Às vezes menina
Que não sabe se expressar.
Mas é só desejo
Reprimido,
Não vivido
Por você e esse teu jeito
Tão chato de tão sensato.
Mas que me excita
Como nunca nada antes
Me incitou.
Você me instiga
E eu me exalto
Porque não conhecia
Esse sobressalto.
É irracional,
E é tão bom.
A razão não alcança
Essa sensação.
Delírios
Eu não os guardo.
Eu os falo.
Ah, essa tua camisa
Ah essa tua cara séria...
Eu me pergunto como seria,
Como você se despiria,
Como você me despiria.
...
Ah, eu não sei se posso ser tua amiga
A minha imaginação sim é minha amiga.