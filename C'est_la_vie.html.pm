#lang pollen
◊define-meta[title]{C'est la vie}
◊define-meta[author]{Elis Bondim}
◊define-meta[date]{05.05.2020}

A vida não é hoje
Menos do que antes;
Qualquer estória que se conte
É só um mero instante.
Porque a vida é tão frágil
A morte é tão fácil
O fim é tão ágil
Quanto sempre foram.
Tal delicadeza tão rude
Persiste além de qualquer coisa que mude,
Insiste em ser inerente à vida.
Hoje está apenas mais nítida
Do que em outros períodos;
Escancarada aos olhos de todos.
O choque é um convite
À consciência do tempo:
Não importa o quanto se evite,
Ele nos escapa pelas mãos
E sempre poderá nos escapar
Imprevisivelmente,
Inesperadamente,
Out of the blue.
E, contudo,
Ainda assim,
Talvez por isso mesmo,
A vida é tão rica
E tão feliz
E intensa.
Imensa.
Tão gigantesca,
Que nela cabem a destruição,
O fim,
E a reconstrução,
A nada exclui.
Por isso se deve todo dia viver,
Para morrer
E se poder dizer:
Fui feliz.
Porque a todo eu,
No momento em que se for,
Só lhe importa que foi.
Ciente de que somos só um oi
No mundo,
Que uma hora se torna
Um adeus.
Por isso eu digo,
Até grito,
Deixo aqui o meu registro:
Quando a minha vez chegar
E a morte me beijar,
Seja por um vírus,
Um acidente,
Ou um tiro,
Eu terei sido
Grande;
Sorriso;
Abraço;
Alma que se expande
A cada passo.
E isso nem importa
A ninguém além de mim mesma.
Mas ao mesmo tempo sou parte
Do todo que comporta
A tudo e a todos.
Um todo cosmológico
Incompreensível,
Inalcançável,
Talvez de algum modo lógico,
Talvez sem qualquer racionalidade,
Talvez metafísico,
Mas que nos alcança
Na escuridão da noite
E na claridade dos raios do Sol.
E certamente é maior
Que a qualquer ente
Perecível,
Breve,
Fugaz,
Pó.
A morte é mais triste
Pra quem não foi feliz.
Para quem respirou
Sem viver,
Quem muito foi matéria
E pouco foi ser
Alma
Não a clássica
Da oposição filosófica
Mas a da poesia
Do sublime estético
Que é viver a cada dia
Não se sendo cético
Quando o assunto é crer
Na beleza do mundo
Organizado no caos
No diverso
Na intempérie
Em cada fim
E em cada início
Cada ciclo
Porque todo não
Pode ser um sim
Depende mais,
Talvez unicamente,
Do ponto de vista,
Da perspectiva,
Que o valora assim.
A esperança não tem de estar perdida,
O amanhã ainda é chance
As possibilidades ainda são infinitas,
Pois a vida não se paralisa
E somos só moléculas
Em movimento
Constante
Natural
Transformadoras
E transformadas.
A vida segue,
Como sempre seguiu.

