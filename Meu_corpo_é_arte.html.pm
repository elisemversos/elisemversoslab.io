#lang pollen
◊define-meta[title]{Meu corpo é arte}
◊define-meta[author]{Elis Bondim}
◊define-meta[date]{12.10.2020}
◊define-meta[audio]{Meu_corpo_é_minha_arte.ogg}

Cachos me cobrem os olhos
E balançam com o vento, como ondas do mar.
Pinceladas constituem os meus poros
E colorem minha pele a me materializar.
Minhas pupilas se dilatam
Com as luzes brilhantes que observo,
Minha saliva me hidrata
E à boca que eu quero.
Meu corpo jorra tintas
Com as quais quero colorir
Aquele por quem eu sinta
O desejo de cola a mim.
Sou, afinal, como colagem
De recortes da vida,
Dos quais me ficam imagens
A constituir filmagem vivida.
Rememoro por fotografias
O que fui;
Crio mosaicos na tentativa
De manter de pé o que o tempo rui.
Minha voz é melodia
Pra ouvidos atentos;
Eu sou toda poesia
Pra quem quiser viver me lendo.
Eu sou uma obra de arte da natureza
Porque eu sou criatura e criadora,
Abraçando a mim mesma
Instaurada instauradora.
E sempre me restauro 
E à minha própria arquitetura,
Sigo os meus passos
Na arte da minha própria conduta.
Sou crua pura e desnuda,
Corpo de formas e cores.
Leveza que não se afunda,
Mas mergulha no mais fundo que encontre.
Sou artesanato feito com minhas mãos,
Não, não sou uma tela em branco,
Mas retrato paisagem abstração 
A serem pintados também por quem, me criando,
Belamente cruzar meu caminho
E se deixar estar, sem tempo contado,
Pra que use seus dedos palavras gestos mais ricos
Pra deixar marcas no já criado,
Mas também continuamente se criando:
Sou convite a quem aprecie o intensificado
Da arte que só existe se entregando
Pra me criar junto a mim,
Um pouco mais e um pouco diferente
Do que já é Elis
No ritmo cor luz forma tom do que se sente.