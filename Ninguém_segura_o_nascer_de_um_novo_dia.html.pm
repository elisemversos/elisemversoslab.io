#lang pollen

◊define-meta[title]{Ninguém segura o nascer de um novo dia}
◊define-meta[author]{Elis Bondim}
◊define-meta[date]{30.07.2020}
Quando um novo raio de sol,
Com sua energia que traz a vida, 
Instaura no céu um arrebol,
Dando ao mundo um novo dia,
Não há força que o impeça
De resplandecer no céu,
Nem cálculo que meça 
Quão tolo seria o papel 
De quem tentasse conter 
O tempo em seu caminho,
Inevitável em seu transcorrer 
E único dono do seu ritmo. 
Nem a nuvem mais carregada
Ou as lágrimas mais tristes, 
Nem doença, vírus ou praga,
Nenhum palpite que se arrisque.
O tempo passa e não pede licença 
Pro bem e pro mal e pra todo querer, 
É persistente de nascença;
Não há nada que se possa fazer. 
Talvez o segredo então 
Seja justamente o de aceitá-lo,
Não travar batalha em vão, 
Mas sim feliz acompanhá-lo.
Entender que o tempo leva 
Assim como ele traz.
E que ele carrega 
Aquilo que ele próprio faz. 
O desespero hoje
Amanhã é ventania, 
O peso das folhas não o interrompe,
E sim lhe fazem companhia. 
As perdas do presente 
Presenteiam as lembranças.
Às vezes a gente não as entende, 
Mas as horas são que nem crianças:
Alegres e brincalhonas, 
Inauguram sorrisos mundo afora.
Porém, também são choronas 
Quando não sabem lidar com o agora. 
Não arredam o pé 
Nem por um decreto.
E têm a força da fé 
Que nos faz buscar o certo. 
E, nessa passagem temporal,
Que é a vida ela mesma, 
Não há dia que seja igual:
Cada minuto é diferença. 
Saber aproveitar
A fluidez tão intrínseca
É saber direcionar 
O foco pro que intensifica.
Viver é estar no tempo
- Breve durabilidade - 
Até que em algum momento
Seremos eternidade.
Afinal, nada segura 
O novo segundo que surge. 
E nada mais dura,
Além do próprio tempo que urge. 
No futuro, estaremos fora do tempo
E não seremos mais vida.
Por isso, é melhor irmos vivendo 
O melhor de cada dia.