#lang pollen
◊define-meta[title]{O que você vê e o que você não vê}
◊define-meta[author]{Elis Bondim}
◊define-meta[date]{12.10.2020}
◊define-meta[audio]{O_que_você_vê_e_o_que_você_não_vê.ogg}

O que você vê e o que você não vê
São peças do mesmo jogo,
Somadas ao que você quer ou não ver,
Ao que te move e como me movo
No movimento das vontades
Que nos impulsionam a vida,
Que aos poucos nos invadem
E nos levam à euforia,
Desde que nos mantenhamos abertos,
Apesar desse mundo de tantas dores,
Sabendo que os caminhos são incertos
E deliciosamente desafiadores.
O desafio de sermos quem somos
E nos emanciparmos dos moldes,
Das projeções dos outros,
Daquilo que nos encolhe,
É o desafio de pegarmos com as nossas mãos
A nossa própria existência
E de corpo, alma e coração
Sermos o que somos sem resistência.

Coragem, é preciso um pouco de coragem
Pra embarcarmos na escuridão
Dessa longa, bela e forte viagem
Em que mergulhamos na nossa própria imensidão.
E nela cabe quem quiser vir junto
E aquilo a que se escolher.
Ainda que, porém entretanto contudo
Nunca tenhamos o controle de a tudo conter.
Ou melhor, devo trocar tais palavras,
E dizer: ademais, além de, sobretudo
Porque há muito que nos escapa
No fundo, quase tudo.
Mas podemos escolher nossa postura
Diante do que a vida nos traz,
Das atitudes que nos constituam,
Dos nossos próprios sinais. 

E a delícia de simplesmente sermos
É a delícia da liberdade de quem abraça
Os próprios e gostosos devaneios
Que nos elevam a alma.
Porque a vida é forte e pulsa
Fazendo-nos pulsar ao vivermos
Aquilo a que se não se recusa
Porque nos expande o pensamento:
Desejar é pensar,
Pensar no que por que a quem se deseja,
Num movimento de se entregar
Ao que vier, do jeito que seja. 
