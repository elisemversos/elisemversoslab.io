#lang pollen
◊define-meta[title]{Essa tua camisa}
◊define-meta[author]{Elis Bondim}
◊define-meta[date]{27.07.2020}
◊define-meta[audio]{Essa_tua_camisa__2_.wav}

Que nem em cena de novela,
Eu cheiro essa tua camisa
Desejando que você esteja nela,
Quando meu rosto dela se aproxima.
A esse pedaço de tecido
Que tenho aqui diante de mim,
Eu dou a ele outro sentido:
O de te sentir ainda aqui.
E, ao te sentir mais presente,
Torno essa mera peça de roupa
Numa lembrança da gente,
Que me anima toda.
Essa tua blusa esquecida
É como um sinal pra você voltar
E eu alimentar a alegria sentida
Quando você está.
É só uma peça de algodão
Com um pouco de poliéster,
Mas sob ela bate teu coração
Quando você a veste.
Tamanho médio,
Azul e cinza,
Cobre a pele que eu quero
Em contato com a minha.
T-shirt clássica
Em um caso fora do normal,
Modelagem básica
Mas sem nenhuma outra igual.
Porque as pessoas sob as outras
São outras também.
E que me interessam as pessoas
Que não são você?
Talvez por alguns minutos,
Elas possam me dizer algo
Ou para prazeres diminutos
Que não têm muito significado.
E isso é até bom, não nego,
O sem importância também é gozo,
O sexo pelo sexo:
Tudo é prazeroso.
Mas o valioso mesmo
É o ser que habita o corpo,
A alma lá dentro
Cuja matéria é só um ponto
De toda a constelação
Que é uma pessoa-estrelas,
As quais eu vejo em teu jeito são
E de beleza intensa.
Nessa tua calmaria,
Que na cama se transforma
Em quase uma selvageria
- a mais gostosa.
Nesses teus olhos que brilham
De um modo que nunca antes vi
E nesses teus dentes que cintilam
Quando você sorri.
Nos teus cabelos que se embaraçam
Junto aos meus 
E nessa tua voz que me abraça
Sem você mover um dedo.
Mas, sobretudo, e muito mais
Nos teus hábitos que me surpreendem,
Pequenas coisas que você faz
Que te fazem diferente.
Essa tua camisa aqui
Lembra-me de tudo isso,
De que B com Elis
Pode ser rico,
Mesmo que seja fugaz,
Mesmo que não haja depois,
Porque nada desfaz
O que já foi.
Eu me apego unicamente ao agora
Porque é tudo que eu tenho,
E agora o que há é esse teu cheiro que aflora
Todo o meu desejo.
