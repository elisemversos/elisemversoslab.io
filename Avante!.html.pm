#lang pollen
◊define-meta[title]{Avante!}
◊define-meta[author]{Elis Bondim}
◊define-meta[date]{27.03.2020}

Eu não vou queimar as tuas cartas,
Tampouco apagar as nossas fotos.
As lembranças a gente não descarta;
Nada mudará o que fomos.  

O amor é um acaso;
Uma sorte em que se persevera.
Mas também tem seu ocaso,
E só o gosto dele se leva. 

O remorso não faz sentido,
Não leva a lugar algum.
Foi belo e infinito,
No tempo em que durou.

Todo fim é recomeço
E recomeço é novo sonho.
Novo sonho é novo meio
Pra sermos o que ainda não somos.

Não somos mais os mesmos 
E aceitar a mudança
É sinal de que crescemos 
E algo pesou na balança. 

Não tem de ter explicação
Mas talvez saibamos que tem. 
Mas também sabemos que teve  coração
Alma, carinho e bem. 

O passado não será reconstruído;
Talvez possa ser revalorado.
Mas que o vivido não seja corrompido
Pelo hoje antes inesperado. 

Avante!