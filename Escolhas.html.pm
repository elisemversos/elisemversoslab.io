#lang pollen
◊define-meta[title]{Escolhas}
◊define-meta[author]{Elis Bondim}
◊define-meta[date]{26.06.2020}
◊define-meta[audio]{Escolhas.wav}

Um match no aplicativo
E uma coragem no peito,
Pra te abrir ao meu íntimo
E enfrentar meus medos.
Foi só obra do acaso,
Mas eu gosto de enfeitar.
Afinal, se há significado,
Ele é criado por quem o conceituar!
E eu sou assim criadora,
Vivendo artisticamente,
Uma alma sonhadora,
Que delira, deseja e sente. 
E criei vários sentidos
Pra semear uma semente,
Pra ver se eu germino
E gero fruto que te contente.
Mas esse pequeno grão
Não tem raízes,
E todo esforço meu foi em vão
Pra que as chances não caíssem
E crescesse uma esperança verde
Numa terra de aridez,
Que não é propícia ao crescer,
E, após esses meses,
Por mais que eu tenha regado
O terreno seco virtual,
O possível na ausência de ato,
Não tá vingando, não tá legal,
E, enfim,
Não está me dando as flores
Que eu plantei em mim
Pra fazer de nós dois amores.
Humano, demasiado humano -
Eu vou ficar doente 
De um jeito ou de outro, pelos cantos,
Tanto que parece ser melhor cortar rente
O que não está dando sinais de vida
Porque não há mais seiva,
Só há o amargo da ilusão perdida,
Que, por mais que eu a queira,
Não está voltando,
Não está bastando,
A balança está pesando
Tanto, tanto...
Que eu já não posso continuar.
Eu me sinto sem escolha
E que esse ato triste a me angustiar
De forçada recolha 
É uma luta contra uma paixão irrealizada,
Interrompida,
Precipitadamente pausada,
Não vivida.
Porque eu escolho a paixão,
Talvez inconseqüente!
E você escolhe a razão,
Muito prudente!
Escolhas
Diferentes 
Pessoas
 - Uma pensa, a outra sente. 