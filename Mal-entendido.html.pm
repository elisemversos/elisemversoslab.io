#lang pollen

◊define-meta[title]{Mal-entendido}
◊define-meta[author]{Elis Bondim}
◊define-meta[date]{16.06.2020}

As palavras têm sentidos independentes
Que pululam descontrolados
Porque não as lemos objetivamente,
Não colocamos o nosso eu de lado.
Nós as direcionamos
Como se fôssemos donos delas,
E nos precipitamos
Em induções às vezes sérias.
Mas pra todo mal-entendido
Pode haver novo perdão,
Se o erro for esclarecido
E for grande o coração.
Eu quero mais que o teu encanto
Porque eu quero me dar toda.
Há falas tuas, no entanto,
Que outro passo me demonstram.
Mas depois você vem
E diz algo diferente
Porque entendeu mal também
Outras frases entre a gente.
É a problemática da linguagem
Não tão bem compartilhada,
Pra além dos signos, as mensagens,
Cobertas por véus e enigmáticas.
Se você sente algo, não o nomeie.
Apenas sinta e profira.
Pra que a gente não se engane,
Mas se permita.
Sentimentos não vêm em caixinhas,
Não têm de ser classificados.
Só precisam ser potência
Que inaugure chance de ato.
Minha declaração não é de amor,
Esse não é o único possível.
Pra que rotular isso, seja o que for,
Se só o que importa é sua força intangível?
Esqueça as etiquetas tradicionais
Porque nem sempre elas funcionam.
Só precisamos da energia que faz
Sentirmos que nosso encontro nos impulsiona. 
Se sentimos borboletas no estômago,
Sabendo que às vezes voam, às vezes pousam,
Lá no fundo, lá no âmago,
Não há palavra que possa dar conta.