#lang pollen
◊define-meta[title]{Conselho}
◊define-meta[author]{Elis Bondim}
◊define-meta[date]{Março 2018}

(A mim mesma e a quem interessar possa)

É preciso entender que dizer ‘eu te amo’ é insuficiente,
Pois o amor é uma energia que se troca...
Quem é amado sabe sê-lo simplesmente porque o sente...
E quem já conhece o amor não o confunde com palavras da boca pra fora.

É preciso entender que as pessoas não te dão o que te é merecido,
Muito menos o que é por ti desejado ou esperado.
As pessoas te dão o que nelas tem contido,
O que há dentro delas consolidado.

As pessoas te dão o que elas são
Em suas mais profundas essências...
Aquilo que semearam em sua interna plantação,
Aquilo que foram capazes de colher de suas vivências...

Não é possível ser verdadeiramente amoroso quem não é amor.
Não dá para ser generoso aquele que não é doação.
Não dá para espalhar aroma de felicidade quem não é flor,
Nem gerar calor humano quem não é paixão.

Não dá para oferecer porto seguro quem não é confiança,
Tampouco ser amigo do peito quem não é coração.
Não dá para ser bem-humorado quem não entra na dança,
Nem ser companheiro quem é prisão.

Há quem acredite fortemente ser grande amigo,
Quando não é nada além de um mal conhecido...
Palavras são insuficientes, tenho dito,
São precisos sentimentos mutuamente sentidos.

Mal entendidos internos, distorções de correspondência entre sentimentos e ações,
Fazem com que pessoas acreditem dedicar o que não dedicam,
Fazem com que pessoas superestimem suas reações,
E não abram mão das crenças em que suas unham afincam. 

Pessoas acreditam dar a outras o que nelas não existe para dar,
Não compreendem o precipício que há entre discursos e atitudes,
Os sentimentos presentes em cada um, que se espalham no ar,
Capazes de reduzir ou ampliar longitudes. 

O autoconhecimento é uma busca incessante,
Cujo objetivo é alcançar a si próprio.
Reconhecer-se é um desafio constante,
Cuja negação tem efeito auto protelatório.

Aceitar tal desafio, aceitar o que as pessoas são,
Aceitar o que nós próprios somos,
Estes sim os maiores reptos em ação,
Contra os quais existe Cronos.

Por isso, aproveite melhor o tempo de sua ampulheta
E busque conhecer os seus próprios conflitos.
Reconhecer os sentimentos picaretas
E conciliar os feitos e ditos. 