#lang pollen
◊define-meta[title]{Paralelismo}
◊define-meta[author]{Elis Bondim}
◊define-meta[date]{25.06.2020}

Seria um erro me deixar dominar
Pelo paralisante pensamento
De que, se tudo tem de um dia acabar,
Pra que iniciar qualquer relacionamento?
Será que vale à pena o esforço,
Se é pra depois ter de deixar ir,
Se é pra um voar sem o outro,
E termos de enfrentar o fim?
Vale à pena o tempo
E toda a dedicação,
Se há tantos planos e intentos
Que são de separação?
Vale à pena se entregar
A quem fala mais alto o ‘si mesmo’,
Muito acima do abraçar
O ‘nós’ e o traçado do seu eixo?
 Caminhos assimétricos
Não são percorridos de mãos dadas.
O estar junto não é télos,
E sim o lado a lado na caminhada.
O desequilíbrio que se instaura
Nos sonhos que ao outro excluem
São força energia e aura
De total falta de saúde.
Nada de bom pode provir
Da condição da desigualdade.
Podemos dispensar sem receio o porvir,
Pois já somos nesse instante duas temporalidades.
O desencontro no tempo
E nas relacionais concepções
Traz dolorosamente ao centro
O cair a ficha das ilusões.
Cada um em seu plano
Capinando o seu trajeto,
Rico ou tacanho,
Paralelos.
