#lang pollen
◊define-meta[title]{Palavras precipitadas}
◊define-meta[author]{Elis Bondim}
◊define-meta[date]{21.10.2020}
◊define-meta[audio]{Palavras_precipitadas.ogg}

Palavras, palavras
Quem cria seus sentidos?
Não somente quem as fala,
Mas também quem interpreta o que foi dito
Conforme a única medida que nos é possível:
A medida de quem se é,
Porque o outro é mundo quase inacessível
E conversar é quase um ato de fé.
Acreditamos entender
E às vezes nem temos ideia,
Temos a intenção de conhecer
Aquilo que nelas não se revela.

Intenções, Intenções
Às vezes parecem não nos levar a lugar algum,
Somente os resultados das ações
De acordo com a interpretação de cada um.
Mas se importam ou não importam,
É fato que uma intenção má pode enganar
E nos abrir portas
Pra precipícios à beira mar.
Enquanto também sobre outras tantas boas
Podemos facilmente nos ludibriar
Sem captarmos por trás de como soam,
Precipitando-nos no pré-julgar.

Precipitações, precipitações
Elas nos retiram antes do esperado
Dos lugares em que as intenções
Não foram bem expressas no ato falado.
E de um susto nos deparamos
Com o que, em nossa ingenuidade,
Não tínhamos nem imaginado,
Talvez numa falta de sagacidade.
E a vida assim se faz
Em pequenas confusões
Que às vezes nos tiram até a paz,
Pro que cada um tem suas razões.

Ah a comunicação, a comunicação
É tão falha precária ilusória!
E nós em nossa louca pretensão
De controlar o espaço e as horas
Perdemos-nos em nossos ditos e não ditos,
Nossas interpretações
Nossos próprios sentidos
Criados em vontades leituras precipitações
Sempre tão idiossincráticas
Às vezes também arbitrárias
Mas também tão sinceras mesmo quando erráticas,
Habitando nossas mentes imaginárias.

Desencontros, desencontros
Viver é a arte de encontrar e desencontrar
A nós mesmos ao lermos o outro
E ao outro ao abrirmos nossas bocas para lhes falar.
Talvez seja essa a mais difícil das trocas:
A troca entre o que não é dito não é expresso,
Não é desejado intencionado não está claro na prosa,
Não é explícito não sabemos não vemos o nexo
Está por trás escondido oculto não se revela 
Não se mostra não nos cabe não é sobre nós,
Mas somos nós que lemos e sobre nos fazemos inventores,
Criando significados desfazendo ou fazendo nós,
Constituindo o caminho de que somos seguidores.

Generosidade, generosidade
Ah só a generosidade em lermos o outro
Com os melhores olhos da nossa própria intimidade,
Só ela pode nos levar a um reencontro
Com esse elemento que se mascara
Sob o que nós mesmos criamos
Ou sob palavras mal colocadas
Nas quais mal nos expressamos.
Ah a generosidade do perdão
Nessa troca sempre incompleta,
Só ela conduz a uma conexão
Que vá pra além da aparência de uma tela.
 
Para. P.T.

