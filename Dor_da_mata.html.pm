#lang pollen
◊define-meta[title]{Dor da Mata}
◊define-meta[author]{Elis Bondim}
◊define-meta[date]{19.08.2020}
(poema escrito para o desafio semanal da Confraria da Poesia Informal)

Causas naturais causas humanas 
Trazem dor à mata,
Que morre entre chamas 
E grita já desfolhada
O grito silencioso 
De quem não é ouvido,
Reduzindo a escombros 
O verde antes tão vívido
E agora cor de fuligem -
Sinal visível do fogo matador 
Que, por menos que se atice, 
Ora e meia espalha a dor. 

Em meio à mata, seres vivos
De diferentes portes 
Vêem seu recanto destruído
E se encontram com a morte 
São devorados pelo calor
Tendo sua pele incinerada
Sentindo toda a dor 
Da natureza ignorada 
Que chora o choro dos inocentes
Em tristeza incomensurável
Lamentando por sua gente 
Que é a base de toda vida imaginável. 

Em meio à mata e sobre a mata 
Nela através dela e sob ela
O homem mata 
Sem se dar conta de que está nela
Toda a sua condição de vida 
Então mata a si ao matar o outro, 
A natureza de que se distancia; 
Só que essa morte lenta é tão aos poucos
Que o homem de hoje mata o de amanhã,
Fechando os olhos pro presente, 
Numa presunção tola e vã
De que tudo será diferente. 

O homem tão racional,
Em seu abissal orgulho,
Se acha isolado do natural 
E não pensa no futuro.
Poluição incêndio caça,
Aspectos de uma mesma atitude:
A de quem maltrata
E estupidamente se ilude
Ao se pensar independente
Da mãe que é berço e direção:
A natureza que nos mata a sede, 
Nos alimenta dá o ar luz e chão. 

Parece até que o homem assassino,
Em vez de preservador,
Impensadamente se confundiu 
E busca preservar a dor;
Em vez da natureza conservador,
Parece que um nó na sua cabeça
O fez erroneamente achar, supor 
Que deve conservar a dor da natureza 
E assim ele segue espalhando 
A destruição à vida,
Como se ele estivesse dispensando
Toda a vida a ser vivida. 

Matar a mata,
Ser matador,
É matar a casa 
Do espírito criador 
De cada ser de ‘cada ente,
Em vez de matar a dor,
Que afeta toda a gente
E ser amante amador,
Que se vê parte de tudo,
Potencial destruidor
Ou construtor do mundo.

Mas os apelos ainda existem,
Ainda há esperança.
Vamos plantar sementes
No desejo vontade ânsia
De que sejam mais significantes
Pra’ novos homens de cada dia, 
Efeitos da natureza naturante,
Capazes de ter consciência 
De que são natureza naturada,
Parte efeito modo da natureza
Divina física simplesmente não separada 
Da humanidade e sua existência. 