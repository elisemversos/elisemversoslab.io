#lang pollen
◊define-meta[title]{Nenhuma a menos: manifesto}
◊define-meta[author]{Elis Bondim}
◊define-meta[date]{12.08.2020}
◊define-meta[audio]{Nenhuma_a_menos.wav}

Por um mundo em que hematomas
Nos corpos e almas de mulheres
Sejam só marca de quem encontra
Prazer em mãos que querem 
Uns apertões gostosos na carne,
No ato de amor concordado;
Inusitado, mas acordo entre as partes,
Trazendo êxtase por ser desejado. 

Por um mundo em que tapas
Não afetem nossos rostos,
Mas no máximo pele que é dada
A batidas que vão de encontro 
A quadris oferecidos,
Com gosto vontade tesão,
Consenso amigo.
Talvez pouco usual, mas por que não? 

Por um mundo em que empurrões
Só se forem de leve pra segurar bem firme,
Puxando de volta e sendo ponte
De conexão e confiança que animem. 
E que todos os puxões
Sejam de cabelos que se dão
Unindo seres que, em explosões
Queimam-se na mais deliciosa conjunção. 

Por um mundo em que só se mate
A sede da saliva de quem se ama, 
Quando em união os dois achem
Bom e gostoso dividir a cama. 
E que só se sufoque 
Por abraço apertado 
Pra sentir o calor e o choque
Dos dois bem colados. 

E, assim, por um mundo em que as mortes
Sejam só de causa natural 
E que a violência que nos tira a sorte 
Seja simplesmente irreal,
Tendo sua existência retirada 
Desse planeta em chamas e injustiça
E, se por ventura desobedecida,
Que tal exceção seja bem punida.

Por um mundo de amor
Em que corpos só se encostem 
Se tal ato por boas energias for,
De modo que eles demonstrem
Só troca positiva,
Enriquecendo vidas,
Em que só se atiram 
Beijos disparados que acarinham. 

Que todo dedo na pele 
Seja de respeito e comunhão,
Sem conduta que fere
A epiderme e o coração. 
Que qualquer arranhão
Seja de unhas nas costas
Só na hora da paixão,
Que só em reciprocidade encostam.

Por um mundo em que não haja
Nenhuma mulher a menos,
Tratada como objeto ou escrava 
Pra satisfazer intentos
Machistas egoístas cruéis,
Que trazem o mal ao cosmos
Sem noção de que isso é
Pura encarnação do ódio. 

Por um mundo em só exista 
Aquilo que soma e intensifica
Os bons sentimentos que aproximam 
E que deixam melhor a vida. 
Por mais sonhos em parceria 
E mais relações saudáveis, 
Em que haja a alegria 
E os atos mais afáveis. 