#lang pollen
◊define-meta[title]{Decisões}
◊define-meta[author]{Elis Bondim}
◊define-meta[date]{16.08.2020}
◊define-meta[audio]{Decisões-clean.mp3}

Eu posso ser luz,
Mas depende de você
E essa escolha conduz 
Tudo que a gente viver. 
O que eu desperto 
É o que você se desperta,
O que você pōe por perto,
Tua escolha poética. 
Porque a poesia é mais
Do que versos escritos,
É o jeito que faz
Toda emoção ter mais brilho. 
Ela é sentimento
Que decidimos dedicar,
Potencializando até os pequenos 
Que estejam a nos adentrar. 
Por isso pra mim é fácil
Tornar cada ato diário,
Acontecimento ágil,
Em poético ato. 
A tudo que eu vejo 
Eu decido transformar
No melhor que tenho em meu seio
E que meus olhos podem enxergar. 
O melhor que posso fazer 
De cada um, porém, 
Não é o mesmo que de você,
Pois ninguém é igual a ninguém. 
Alguns nos afetam de um modo
E outros de outros tantos. 
Mas se eu destaco ou descarto,
Se eu realço o que tô desejando
Com uma cor fosforescente
Pra marcar a minha visão 
Ou só sinal do lado ou na frente,
Fácil de escapar da minha direção,
Isso é decisão,
Decisão decidida 
Que decide no coração 
Oferecer-se ao outro moradia. 
Quero te dar o meu melhor
Pra você perceber 
Que quem faz de mim sol
E belo amanhecer
É o olhar que me olha 
E que decide me fazer
Mais do alguém que se gosta 
E paisagem que se vê. 
Não depende de mim 
Ser mais do que sou,
Só depende de ti,
Você que tem de ser amor. 
Só se pode dar ao outro 
O que já se tem em si;
E, mesmo assim, no entanto,
É preciso decidir. 
Eu decidi no meio do caminho 
Porque acreditei em alguns gestos 
Palavras frases cujo sentido 
Ainda são os que quero.
Mas aos poucos me dei conta
De que decidi sozinha 
Porque a entrega não foi toda
Nem a segurança era rainha;
Que aquelas letrinhas,
Que juntinhas diziam 
Terem sido tão refletidas,
Não o foram na medida. 
As dúvidas já existiam
E permanecem presentes,
Dúvidas que angustiam 
E que se colocam entre a gente.
Essas dúvidas são menos
Sobre números e experiências, 
Que ajudam no intento,
Mas são só parte da busca inteira
Que se faz necessária
No interior de si próprio
Pra se entender que a passagem 
Que eu sou a assopro
Pode se deixar estar 
Diante dos teus olhos
E isso é determinação tua,
A qual é determinante de nós. 
Fazer-me alvorecer ou lua,
Que brilha na tua vida,
Que esclarece e inaugura,
Ou que quase não é vista 
Nos teus horários tão diurnos,
No céu ali tão linda, 
Mas distante do teu mundo. 
Somos o que escolhemos
E sentir também é escolha,
Sentir mesmo não entendendo,
Mas com a determinação toda
Que faz acontecer 
O que se mentaliza,
O que se diz querer,
O que se prioriza. 
Claro que não escolhemos 
Exatamente cada sentimento,
Mas há decisão no pensamento
De intensificar e ter o intento,
De transformar aquilo que surge,
Tornando forte o não tão forte, 
Especial o que se curte 
E oportunidade a sorte. 
Assim como se pode 
Transformar mágoa em ódio,
Semente em fruto que se colhe,
E 3o lugar em pódio, 
Também se decide um bocado 
Se se quer intensificar o que acontece 
Ou deixar assim meio fraco,
Sem olhar que enternece. 
Decisões são determinantes,
Determinação intensifica, 
Escolhas farão da gente
Brevidade ou vida. 