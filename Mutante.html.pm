#lang pollen
◊define-meta[title]{Mutante}
◊define-meta[author]{Elis Bondim}
◊define-meta[date]{19.03.2020}

Expulsei da minha vida
Toda a autopiedade
E num lance de autoestima
Assumi a minha responsabilidade.

Eu estou me emancipando
De mim mesma:
Amarras antes me segurando
Cuja ausência me reinventa.

Eu não quero seguir as regras;
Eu flerto com a desordem.
Eu gosto de andar às cegas;
Quando o inesperado me morde.

Eu não desejo o conveniente;
Eu gosto é do incomum.
Não há contras quando a gente
É em si inteiro como o um.

O estável não me atrai tanto,
Nem a repetição do mesmo,
Se não for tão intenso quanto,
Como uma trilha em que me perco:

Eu sei o caminho
Mas quero percorrê-lo de outro jeito.
Que o mapa seja perdido
Ou eu o vire do avesso.

Porque eu gosto de me desencontrar
E que o previsível passe longe,
Pra me encontrar em outro lugar
Que jorre fluxos como uma fonte.

Esses fluxos são energia
Violenta porém vital
Destroem toda monotonia
Constroem um outro e desigual.

Que eu seja a cada dia
Um pouco mais diferente
E que eu não perca a magia
Da mudança intermitente.

Que o movimento seja o padrão
Único a me guiar
A passos largos salto do chão,
A passos curtos observo o que há.

E nesse movimento da vida
Vou me alterando a cada instante
Aberta e certa sem dúvida
À mudança mutante.