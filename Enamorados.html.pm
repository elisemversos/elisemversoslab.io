#lang pollen
◊define-meta[title]{Enamorados}
◊define-meta[author]{Elis Bondim}
◊define-meta[date]{Junho 2020}


Proponho que não estejamos enamorados
Mas assim vivamos:
De modo sempre apaixonado
Pela vida e seus encantos.
É verdade que nela há dores,
Doenças, maldade, catástrofes.
Porém no caminho sempre há flores
Pássaros, sol, lua e árvores.
E quando olhamos com amor
Pra cada singela singularidade,
Seja lá aonde formos,
Levamos a mais poderosa capacidade
Que é a de espalharmos a esperança
O carinho para com o mundo,
A semente de qualquer mudança
Que possa dar frutos no futuro.
O amor é mais que um sentimento
Que nutrimos por outrem;
Ele é alma em contentamento,
Que não se contém.
É um modo de estar na vida,
Um jeito de ser ilimitado,
Um estar junto com a alegria
De simplesmente estar presente no presenciado.
Amor é algo que se faz todo dia,
Através dos olhares, palavras, atos.
É a cotidiana poesia
Dos gestos, atitudes e cuidados.
Proponho que namoremos
Cada centímetro de terra,
Até as cores que não vemos
E a claridade ou escuridão que cegam;
Cada gota dos oceanos
E cada grão de areia,
Como metáforas em que mergulhamos
Ansiando que nos transcendam;
Que amemos o calor do fogo
E o frio do branco gelo,
E que o sentir nunca seja pouco
E sempre escolhamos vivê-lo.
Assim, olharemos nos olhos
De quem nos fortalece mais ainda esse amor,
Esse que já constitui os nossos poros,
Com a energia que nos permite propormos
Que façamos amor no abraço,
Que sejamos amor de mãos dadas,
Que o transbordemos nos percalços
E o reafirmemos nas encruzilhadas.
Que nos reconheçamos
Como pessoas-amor
E, logo, também sejamos
Amor de pessoas aonde formos