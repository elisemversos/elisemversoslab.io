#lang pollen
◊define-meta[title]{Chuva}
◊define-meta[author]{Elis Bondim}
◊define-meta[date]{25.06.2020}
◊define-meta[audio]{Chuva.aac}


Paixões vêm e vão,
Sangrando na mesma medida
Em que explodem o coração
E enchem a casa de alegria.
Às vezes a distância física se coloca,
Impedindo o tato.
Ou se dá de outra forma,
No rompimento emocional do sonhado.
As rupturas são rachaduras
Pelas quais passam o ar a água a luz,
Mas também a dor dura
De corpos vivos e crus.
O cheiro ocre da desilusão
Embrulha o estômago,
E as lágrimas pelo chão
São breves e fortes tal como relâmpago,
Que ilumina só pra escurecer,
Traz o susto pra limpar o céu,
Alivia logo após quase ensurdecer,
Tão natural e temido como cruel.
Mas é só um presságio
Da chuva que chega,
Que transborda os riachos,
Alaga as alamedas,
E faz crescer as flores.
É força violenta e abrupta
E condição de todos os amores,
Traz uma raiva obtusa,
Complexa e ambígua
Por ser fonte dupla
Do encanto e da angústia.
A tempestade destrói
Pra deixar surgir o novo.
É uma só e a mesma que corrói,
Que nos leva aos escombros
E ao mesmo tempo eleva
O sabor dos alimentos,
A palavra mais sincera,
E a intensidade dos sentimentos.
O risco do fim
E da não continuidade
Bate à porta assim
Até na ingenuidade
Através de uma frase mal colocada,
Um descompasso furioso,
Uma carência exacerbada,
Um desequilíbrio insidioso,
Um desencontro temporal,
Um até logo tornado adeus,
Um mal hábito que parecia banal,
Uma desarmonia entre dois 'eus'.
Porque cada um é um eu um mundo uma riqueza
Tão belos em sua diversidade,
Tão comuns em suas fraquezas,
Tão fortes em sua singularidade.
Paixões ferem e brilham
Presas na garganta
Queremos berrar: "fica!"
Ao mesmo tempo que o peito nos arranca!
Sou tomada por essa traiçoeira energia
E não tá fácil de lidar.
Eu já me perdi nessa trilha
E já não sei como voltar.
A chuva vira granizo
Me congela e bate nas costas,
E eu penso que depois amenizo
Essa pancada que a mim se choca.
Mas é pura bobagem
Porque eu não tenho esse poder.
Só o amanhã apaga as mensagens
Trazidas pelo anseio imaturo por viver.
Depois de uma noite inteira
De precipitação intermitente,
O sol aparece mesmo que não se queira,
E um novo dia traz nova semente
A ser levada pelos ventos do acaso
Pra quem sabe um dia se aninhar
Em meio à terra ou ao cascalho
Vindo ou não a vingar.
E nesse ritmo variado
Mas de passo ininterrupto,
Eu, como um ser apaixonado,
Vou me dando ao mundo.