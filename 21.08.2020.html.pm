#lang pollen
◊define-meta[title]{21.08.2020}
◊define-meta[author]{Elis Bondim}
◊define-meta[date]{21.08.2020}
◊define-meta[audio]{Hoje_sao_21_.wav}

Hoje senti tua falta ao acordar 
E do cheiro de manteiga
Misturado com o teu pelo ar
A se espalhar pela casa inteira.
Tua voz, teu 'bom dia, chérie',
Até de você batendo a porta da geladeira,
De te olhar e querer dividir,
Ser teu pão tua luz tua parceira. 
Senti falta de te observar 
Chupando tua laranja.
Só de lembrar me sinto molhar
Por esse caldo suco canja. 
Senti falta do teu abraço
A me circundar na cama
E até do teu hálito,
Cuja boca me chama,
Me atrai me hipnotiza
Com esses teus lábios finos,
Teus dentes brancos que brilham,
E tua saliva doce com a qual sinto
Um gostinho de paraíso
Nessa terra tão complicada,
Cheia de tanta gente sem juízo,
E almas desencontradas. 
Senti falta do modo como você me beija,
Como se beija a alguém a quem se ama.
Mas por automatismo ou porque me deseja,
Porque me quer na vida ou só na cama? 
Não sei, mas te admiro, honey.
Só que você pode ser e dar bem mais:
O que te soma e não te some,
Ciente das escolhas que faz.
Achar a si em si para si,
Pro melhor de si poder dar.
E, por outro lado, poder assim
Receber também o melhor que há.
Eu senti falta essa manhã
Da tua presença na minha casa,
Mas não posso viver nesse afã,
Sem saber se sou affair que passa 
Ou semente de amor-perfeito.
Não tão perfeito, pois real 
E o real não é desse jeito. 
Mas é sombra abrigo fruto flor açúcar e sal,
Sal da vida que dá mais gosto,
Tempero que agrada ao paladar,
Açúcar néctar que mela o corpo
E nos faz lamber os beiços sem nem pensar,
Na espontaneidade do desejo,
Que nos eleva a outro patamar,
Agarrando-nos a cada ensejo 
Que nos dê a chance de intensificar.  
Vai e voa e só volta
Se for pra ficar,
Pra ficar pra dentro dessa porta
Sendo amor amante amado amar. 