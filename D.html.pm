#lang pollen
◊define-meta[title]{D}
◊define-meta[author]{Elis Bondim}
◊define-meta[date]{20.01.2020}

Eu voei no teu sorriso
E admirei tua inteligência.
Imaginei tua voz ao meu ouvido
E me despi de toda inocência. 

Desejei o paraíso
Que é tua pele morena
E lá quis ter perdido
Todo pudor que bloqueia.

Eu quis nadar na tua boca
E te mentalizei sem receio. 
Paguei toda a conta
Do risco do meu devaneio. 

Eu sonhei com o teu olhar
Me desconstruindo.
Quis te desvendar
E te dizer o que acho lindo. 

Desejei teu apreço
Porque te gosto.
E até estremeço
Quando te encosto. 

Eu me narro pra ti
E me invento outra vez
Na esperança de te ouvir
Quem sabe n'outro mês...

No fundo eu só queria 
Te desvelar.
O que você faria?
O que fará?

Conhecer e conversar
Com quem acho interessante.
Sentir que ainda há
Um fio de vida pulsante.

Mas o que não acontece na vida
Não é fato. 
A gente só imagina
E se vence pelo cansaço. 