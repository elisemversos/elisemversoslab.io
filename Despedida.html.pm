#lang pollen
◊define-meta[title]{Despedida}
◊define-meta[author]{Elis Bondim}
◊define-meta[date]{21.03.2020}

Despeço-me daquela vida
Como quem se despede de um amigo
Que esteja de partida
Para viver num país longínquo;

De quem sentirei saudades,
Sem esperança de retorno,
Sem expectativa de que a amizade
Não sofra nenhum transtorno.

Com amor pelo que passou
E ainda mais amor pelo que virá.
Com a certeza de que sou
Resultado do ato de somar.

Eu o deixo pra trás
Pra me levar adiante.
Abro mão do 'jamais'
Pra me abrir pro restante.

A vida é este instante.
Este instante é aqui e agora.
E que o eu tão cambiante,
Seja melhor do que antes fora.

E a beleza da mudança
É maior que a do estático,
Nesta breve intensa andança
Pelo mundo tão novo e ágil.

Que a certeza não me defina;
Que o eterno não me limite;
Que todo fim seja vida
E o viver me desafie.

Meu lema é o movimento,
A cadência da metamorfose.
Em ritmo acelerado ou lento
- caminhando atenta e forte.

Vou seguindo não teus passos
Nem de outrem, mas os meus
Que não seguem percurso marcado
- abrem nova trilha sem medo.

Cortando rente erva daninha
E plantando flores pelo caminho;
E que o verde de toda árvore altiva
me oriente ao meu destino.

Mantendo meus pés no chão
E o olhar pro céu azul.
Se o corpo não levanta vôo,
A alma dá saltos no escuro:

Eu sou minha própria luz
E minha luz me basta.
Meu ser é claro e nu
Como o Sol estelar.