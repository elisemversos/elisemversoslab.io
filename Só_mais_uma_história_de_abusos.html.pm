#lang pollen
◊define-meta[title]{Só mais uma história de abusos}
◊define-meta[author]{Elis Bondim}
◊define-meta[date]{13.10.2020}

Aos 5 um vizinho me mostrou o pau dele
E disse pra eu chupar.
Saí correndo me acolher entre as paredes
Da casa em que eu estava a morar.
Aos 8 ou 9 meu querido familiar
Se sentiu no direito de usar as mãos 
Pra se divertir a me tocar 
Onde ele não devia não. 
No início da adolescência,
Tentei contar a uma pessoa amada 
Que o tal me dava selinhos sem inocência
E ela não acreditou na minha palavra.
Porque nós mulheres somos sempre as mentirosas 
E crianças são as que inventam tudo.
Ouvimos as coisas mais horrorosas 
Até desejarmos que o mundo fosse mudo. 
Não acreditamos em nós mesmas
E nos acusamos impiedosamente.
Somos ensinadas a sentar à mesa,
Mas não a nos amarmos entre a gente. 
No ensino médio quantos não se gabaram
De terem comido a quem eles nunca comeram?
Ah o ensino médio, essa época babaca.
Será que eles se esqueceram?
Porque eu não me esqueci. 
E vivo todo dia a lidar com meus traumas,
Tentando modificar a mim 
E a minha relação entre meu corpo e alma. 
E o pior que a mentira sobre o sexo não realizado
É a de que ele nunca deveria ser 
Motivo pra chacota desprezo olhar mal encarado,
Pois é vida que pulsa e nos faz nascer!
Ele nos acorda pro prazer,
Pra valores naturais,
Pra chance de amor entre dois seres,
Pra momentos belos e singulares. 
O sexo é potência,
Mas nesse mundo querem nos oprimir,
Tirando-nos toda a crença
Nessa força a nos constituir.
Depois os homens querem se sentir
Os tais os caras os bons de cama,
Na doce ilusão de uma imagem de si
Que é a pura ficção e que não encanta. 
Depois não sabem por que 
Geral do sexo feminino 
Não goza nos momentos de prazer,
Só geme alto fingindo. 
Eu não vou mais viver subjugada
Ao que fizeram comigo a vida inteira,
Eu não vou mais entrar nessa furada:
Tô me emancipando de mim mesma. 
O meu passado é o que passou
E eu não vou nega-lo,
Eu não vou fingir ser o que não sou,
Eu vou é confessa-lo. 
Eu não vou mais me calar
Nem me esconder,
Vou dar minha cara a esbofetear
Porque eu tô firme no que quero ser.
E o que eu quero é bem diferente
Daquela que mal conseguia querer:
Eu quero invadir minha mente,
Afogar o que foi e recriar o meu ser.
