#lang pollen
◊define-meta[title]{Nostalgia do dia}
◊define-meta[author]{Elis Bondim}
◊define-meta[date]{21.08.2020}
(Para B. Cuconato Claro)

Estou ouvindo o som da chuva
Sabendo que, dentre todas as presenças,
A que eu mais queria era a tua.
E, dentre todas as faces que eu veja, 
São teus os olhos que os meus buscam 
E que no fundo o que eu mais queria
Era que junto aqui dessa lua 
Você, em plena alegria,
Ainda que escondida entre as nuvens,
Estivesse e me abraçasse
Com uma vontade que ruge. 
E que entre minhas pernas você se aninhasse
E escolhesse fazer abrigo onde moro,
Desejando estar comigo
Nesse dia chuvoso em que quase choro
Por não estar contigo. 
Mas hoje fico no quase
Em vez de me escorrer 
Porque há uma nova face 
Em não estar com você.
É um rosto de alívio
Por não estar sendo observada
Pelo homem a quem eu admiro,
Mas que é a insegurança personificada.
Quero que você esteja comigo
Mas com todo o coração,
Entregando-se ao instinto 
E perdendo um pouco a razão. 
Quero que você decida me amar
Do mesmo modo como se decide sair de casa
Vestir-se, no elevador adentrar 
E dar uma caminhada. 
De modo simples, sem pompa,
Sem sinos tocando ao fundo,
Sem estupor que assombra,
Mas com o querer mais profundo
De quem sabe o que quer 
E que quer ser feliz 
Ao lado de quem o outro é,
Ao lado da tua Elis. 
Quero que você escolha me amar,
Com a tua alma que me é quase amada
E assertivamente queira se deixar estar 
Pra ser amado com a imensidão do mar. 
Eu quero te fazer conhecer
O amor mais forte.
Eu tenho isso pra dar a você,
Mas isso só em conjunto ocorre.
Hoje o dia está cinza, o céu bem Brunet
Sem sol, sem sinal Claro 
Sem brilho toque som ou tv.
Não há cuco-canoro
Voando pelo ar,
Não há beijo gostoso
Não há você, não há.
Há somente eu e meus gatos 
E a água molhando a janela,
Tua garrafa no meu armário
- e sinto sede de você só de olhar pra ela -,
Teu livro na minha estante,
E teu tapa olho diário
Onde você já esteve antes,
Ao meu lado deitado,
Semana passada 
Semana retrasada, 
Mas hoje é memória relembrada
E saudade lamentada. 
