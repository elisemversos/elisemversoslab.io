#lang pollen
◊define-meta[title]{Fantasiei}
◊define-meta[author]{Elis Bondim}
◊define-meta[date]{Março 2020}


Eu criei toda uma fantasia
Com você
Em que tuas mãos nas minhas,
Com o aval do meu querer,
Seguram-nas contra a parede
Acima de mim,
Dando-me mais sede
Do teu sim.
Você me suspende,
E eu, sobre uma bancada,
Banco o teu olhar pungente
Que me deixa dilacerada.
Mil beijos me sugam
Nessa fantasia. 
Meus olhares te estudam
Mais que a toda a filosofia.
Entre minhas pernas
Você se coloca de pé
E nas próximas cenas
Cabe tudo que você quiser.
De um salto eu desço
Pra de joelhos me agachar
E quase me esqueço
Da realidade a me chamar.
Porque quando eu penso
No teu abdome -
Escadas pro paraíso imenso,
Que é teu cheiro de homem -
Eu me perco do mundo
E preciso retomar a razão
Pra lembrar que, disso tudo,
Sou a única a tirar os pés do chão.
É só uma fantasia
Que alimento,
Pra me sentir mais viva
Na falta do teu alento. 