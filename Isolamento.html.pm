#lang pollen
◊define-meta[title]{Isolamento}
◊define-meta[author]{Elis Bondim}
◊define-meta[date]{14-06-2020}

I - 
Nunca senti tanto desejo.
E nunca antes me senti tão longe
E tão triste por não poder vivê-lo.
A vida traz contradições,
Extraordinários paradoxos,
Sentimentais inquietações,
Entre as quais se escondem
Potentes possibilidades,
¬Simultaneamente fontes
Dos mais conflituosos instintos
De preservação
E de entrega ao imprevisto,
De respeito ao outro
E de grito por mais.
E, no entanto,
As dúvidas são riqueza
De toda a vida humana,
Que trazem a beleza
Da roda da fortuna,
Do acaso do cosmos;
E não importa o que ela una
Ela também pode separar,
A qualquer piscar de olhos,
A cada dia em que o sol raiar.
O céu que aproxima
É o mesmo sob o qual 
Dois se distanciam.
E as forças se dissipam
Pro bem e pro mal.
Assim é a vida
Em seu ciclo natural,
A cada anoitecer,
Nessa repetição nunca igual,
Que novamente se renova 
No brilho do lusco-fusco,
E do mesmo jeito vai embora
No irrepetível fluxo
Do que não volta 
E segue seu curso. 
II – 
O tão subjetivo eu
Não deixa de existir,
Não se perdeu
Em prol do tão coletivo todo
Do qual somos parte.
E que, ao mesmo tempo,
Não nos basta,
Não dá conta,
É pouco e esmaga.
Apesar de ser também condição
A todo surgimento individual,
É uma enorme escuridão
Abstrata e sem realidade própria
Que não nos alcança
Em nossa singular história.
A parte que nos cabe no todo
Não nos cabe a nós mesmos,
Porque egoisticamente somos
Muito maiores do que o mundo
E nos sentimos presos.
Ainda assim, porém, contudo
Constituímos inevitavelmente esse globo
E seria no mínimo ingenuidade,
Talvez até tolo,
Não enxergarmos nossa pequenez
Dentro do universo inteiro
E a nossa mesquinhez
Em não colocar em lugar primeiro
Toda a comunidade
À qual pertencemos
E com a qual comungamos 
Toda essa realidade
À qual hoje nos deparamos. 
O isolamento é um desafio
E cada um lida de um modo.
Ele não é um inimigo,
Mas é um buraco na terra
No qual alguns se afundam
E outros plantam ervas
Que crescem e alimentam
Bocas famintas 
Por seivas, sedentas,
Quase perdidas.
Mas que se acham,
Ao achar a luz
Em direção à qual dançam.

III – 
As minhas plantas são emocionais,
Que florescem em poemas,
Combinações frasais,
Que me nutrem a ponto de que ainda acredito
Que as coisas irão melhorar
E que essa criatura invisível
Vai de algum modo nos deixar
Caminharmos ao ar livre
E termos prazer ao respirar. 
Assim como nos beijarmos
Sem receios,
Nos abraçarmos
E nos sentirmos
Sem medo,
Sem imperativo categórico.
Só não tenho como impedir
Esse sentimento,
Que me faz querer sair
E quebrar esse distanciamento,
Que me consome
Por 3 meses sem teu cheiro.
E me invade,
Trazendo um desequilíbrio
Que se chama ‘saudade’.
Ela tem dois lados:
O que alimenta o amanhã
E o que nos faz odiá-lo.
Porque queremos o hoje
Ansiosamente,
Como se ele fosse
A última chance.
E talvez seja,
Como saber?
Não existem certezas.
São cinco da tarde
Ao escrever esse verso.
E logo mais se vai
Mais um dia 
Entre tantos
Nessa monotonia.
Posso acordar diferente,
Posso não acordar,
Só o que não muda é o ontem.
Mesmo sobre ele, todavia,
Podemos criar outros sentidos
Enfeitá-lo com um pouco de magia
De modo a nos sentirmos 
Um pouco mais felizes
Com o passado que já se esvaiu
E ao mesmo tempo ainda marca suas diretrizes.
Já estou na quarta página
De devaneios conectados
E, adivinha?
O que os conecta
É uma mesma e única vontade,
A única da qual no momento estou certa,
Que é a de estar com você
Em corpo, alma e estrelas,
Verdade, entrega e prazer.
O isolamento,
Eu o sigo por razão.
Mas sou alma que vaga sem rodeios
Na imaginação,
A qual me gruda na tua pele
E te leva meu coração.


14.06.2020
