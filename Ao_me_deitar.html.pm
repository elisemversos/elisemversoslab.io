#lang pollen
◊define-meta[title]{Ao me deitar}
◊define-meta[author]{Elis Bondim}
◊define-meta[date]{Dezembro 2019}

Demoro a dormir
Com o sono agitado,
Imaginando o teu toque
De olhos fechados.

Tua barba roçando,
Tua boca na minha.
Língua molhando,
Arrepiando a espinha.

O deslizar dos teus dedos,
Delineando meu corpo,
Dissipando os medos
Como num sopro.

Entre minhas pernas
Pulsa o desejo.
Pulsões internas
Me tiram do eixo.

Imagino teu sexo
De encontro ao meu,
Encontro e reflexo
Meu corpo com o teu.

Imagino o ato
Em cada etapa.
Teu gosto, teu tato
Nada me escapa.

Adormeço pensando,
Tua imagem na mente.
Sonho enquanto
Não acontece entre a gente.