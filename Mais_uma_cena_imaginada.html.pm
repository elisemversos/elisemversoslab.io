#lang pollen
◊define-meta[title]{Mais uma cena imaginada}
◊define-meta[author]{Elis Bondim}
◊define-meta[date]{01.10.2020}

Estou aqui, pra variar, 
Com uma taça de vinho nas mãos,
Prestes a me conectar
Com a minha própria imaginação.
Nela eu te imagino aqui
E os beijos nossos,
E logo quero que essa camisola que cobre a mim
Caia bem diante dos teus olhos.
Pra que você sinta o meu gosto
Com a tua língua,
Que eu quero a percorrer meu corpo,
Cada centímetro da minha pele nua.
E que eu também sinta o teu gosto
Da tua pele mais íntima,
Aquela cujo gozo
Jorra na sua forma líquida.
O que eu quero bem na minha boca
No fundo não é essa taça,
É aquilo que entre tuas coxas
É presença desejada.
No ritmo em que te aprouver
E que você segure os meus cabelos,
E me faça fazer o que você quiser
Sendo do teu prazer instrumento.
Ah esse gosto, eu o queria
Bem agora misturado
À minha doce saliva
Sedenta do que lhe for jorrado.
Junto aos taninos
E da acidez equilibrada
Desse vinho tinto
Que me deixa embriagada,
Eu quero o que é teu
Sentindo o meu tato,
Dentro do que é meu
Na satisfação do contato.

