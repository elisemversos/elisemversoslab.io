#lang pollen
◊define-meta[title]{Gota}
◊define-meta[author]{Elis Bondim}
◊define-meta[date]{Janeiro 2020}
◊define-meta[audio]{Gota_.wav}

Você bem que podia ser
Essa gotinha de suor,
Que escorreu entre meus cabelos
E se aninhou entre meus seios.

Você bem que podia percorrer
Minhas curvas cavas cantos,
Como essa gotinha de sal
Que desbrava meus encantos.

Você bem que podia querer
Ser como essa gotinha d'água,
Que desliza pelo meu corpo
Até os pés onde deságua.

Você bem que podia me molhar
Como essa gotinha líquida
E eu mesma ia beijar
Meu corpo pra te ter na língua.

Você bem que podia estar
Como gotinha de saliva,
Dentro da minha boca -
Uma secreção lasciva.

Você bem que podia,
Mas não é e não está.
Quem sabe outro dia,
Estou aqui a imaginar...



